#!/usr/bin/env bash

set -e
set -u
set -x

API_SERVER=${API_SERVER:-"https://staging.ryax.io"}
API_VERSION=${API_VERSION:-"0.0.0"}
OPENAPI_BIN=${OPENAPI_BIN:-"openapi-generator"}

echo "Download swaggers from $API_SERVER"

curl -Lfk $API_SERVER/repository/docs/swagger.json > repository-spec.json
curl -Lfk $API_SERVER/studio/docs/swagger.json > studio-spec.json
curl -Lfk $API_SERVER/authorization/docs/swagger.json > authorization-spec.json
curl -Lfk $API_SERVER/runner/docs/swagger.json > runner-spec.json

echo "Check input specs"
$OPENAPI_BIN validate -i studio-spec.json
$OPENAPI_BIN validate -i repository-spec.json
$OPENAPI_BIN validate -i authorization-spec.json
$OPENAPI_BIN validate -i runner-spec.json


echo "Merge specs to version $API_VERSION"
python merge-api.py > ryax-spec.json

echo "Generate output with openapi-generator installed locally (Requires version >= 5.0.0)"
$OPENAPI_BIN generate -i ryax-spec.json --package-name ryax_sdk -g python --additional-properties=generateSourceCodeOnly=true
rm -rf docs
mv ryax_sdk/docs .
rm -rf ryax_sdk/test

echo "DONE! The spec $API_VERSION is here: ryax-spec.json"
