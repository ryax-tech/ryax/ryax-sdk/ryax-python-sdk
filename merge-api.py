import json
import os

api_version = os.environ.get("API_VERSION", "0.0.0")

def load_specs(filename):
    with open(filename) as spec:
        spec_dict = json.load(spec)
    return spec_dict


def replace_ref(prefix, spec):
    if isinstance(spec, dict):
        for key, value in spec.items():
            if key == "$ref":
                fixed_part = value.split("/")[:-1]
                prefixed_value = prefix + value.split("/")[-1]
                spec[key] = "/".join([*fixed_part, prefixed_value])
            else:
                replace_ref(prefix, value)
    if isinstance(spec, list):
        for elem in spec:
            replace_ref(prefix, elem)


specs = {}
specs["repository"] = load_specs("./repository-spec.json")
specs["studio"] = load_specs("./studio-spec.json")
specs["authorization"] = load_specs("./authorization-spec.json")
specs["runner"] = load_specs("./runner-spec.json")

final_spec = {
    "info": {
        "title": "Ryax API for all services",
        "version": api_version
    },
    "host": "app.ryax.tech",
    "schemes": ["https"],
    "paths": {},
    "definitions": {},
    "swagger": "2.0",
    "securityDefinitions": {
        "bearer": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header",
            "description": "Ryax token"
        }
    },
    "security": [
        {
            "bearer": []
        }
    ],
}

for spec_name, spec in specs.items():
    def_prefix = spec_name.capitalize()
    replace_ref(def_prefix, spec)

    if spec_name == "runner":
        prefix = ""
    else:
        prefix = "/" + spec_name
    for path, entry in spec["paths"].items():
        if path in final_spec:
            raise Exception(
                f"Path {path} is alredy present in the path: Skipping")
        final_spec["paths"][prefix + path] = entry

    for definition, entry in spec["definitions"].items():
        if definition in final_spec:
            raise Exception(
                f"Definition {definition} is alredy present in the definitions: Skipping")
        final_spec["definitions"][def_prefix + definition] = entry

"""
**WARNING** The studio apispec is not able to provide a type `file` so we have
to change it by hand for the studio workflow export endpoint.  To do so, you
have to patch the ryax-spec.json file by adding `"schema": {"type": "file"}` to
the `"/studio/workflows/{workflow_id}/export"` endpoint for the head AND the
get 200 response.
See https://github.com/maximdanilchenko/aiohttp-apispec/issues/91#issuecomment-804235599
for more details.
"""
final_spec["paths"]["/studio/workflows/{workflow_id}/export"]["head"]["responses"]["200"]["schema"] = {"type": "file"}
final_spec["paths"]["/studio/workflows/{workflow_id}/export"]["get"]["responses"]["200"]["schema"] = {"type": "file"}


print(json.dumps(final_spec, indent=2))
