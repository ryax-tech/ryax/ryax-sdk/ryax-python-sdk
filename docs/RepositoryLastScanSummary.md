# RepositoryLastScanSummary


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actions_building** | **int** | Number of actions currently building | [optional] 
**actions_scanned** | **int** | Number of scanned actions | [optional] 
**scan_date** | **datetime** | Git Repo last scan date | [optional] 
**actions_built** | **int** | Number of built actions | [optional] 
**actions_error** | **int** | Number of actions with errored scan or build | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


