# StudioWorkflowResult


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **str** | Name of the result | 
**workflow_module_io_id** | **str** | Identifier of the workflow action io associated. | 
**description** | **str** | Textual description of the action IO | [optional] 
**type** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | IO type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


