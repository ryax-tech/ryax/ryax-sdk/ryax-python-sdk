# ryax_sdk.WorkflowsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**runner_portals_workflow_definition_id_get**](WorkflowsApi.md#runner_portals_workflow_definition_id_get) | **GET** /runner/portals/{workflow_definition_id} | Get a portal
[**studio_v2_workflows_workflow_id_endpoint_get**](WorkflowsApi.md#studio_v2_workflows_workflow_id_endpoint_get) | **GET** /studio/v2/workflows/{workflow_id}/endpoint | Get workflow endpoint
[**studio_v2_workflows_workflow_id_endpoint_head**](WorkflowsApi.md#studio_v2_workflows_workflow_id_endpoint_head) | **HEAD** /studio/v2/workflows/{workflow_id}/endpoint | Get workflow endpoint
[**studio_v2_workflows_workflow_id_endpoint_put**](WorkflowsApi.md#studio_v2_workflows_workflow_id_endpoint_put) | **PUT** /studio/v2/workflows/{workflow_id}/endpoint | Update workflow endpoint
[**studio_v2_workflows_workflow_id_get**](WorkflowsApi.md#studio_v2_workflows_workflow_id_get) | **GET** /studio/v2/workflows/{workflow_id} | Get one workflow with details
[**studio_v2_workflows_workflow_id_links_put**](WorkflowsApi.md#studio_v2_workflows_workflow_id_links_put) | **PUT** /studio/v2/workflows/{workflow_id}/links | Update workflow links
[**studio_v2_workflows_workflow_id_results_get**](WorkflowsApi.md#studio_v2_workflows_workflow_id_results_get) | **GET** /studio/v2/workflows/{workflow_id}/results | Get workflow results
[**studio_v2_workflows_workflow_id_results_head**](WorkflowsApi.md#studio_v2_workflows_workflow_id_results_head) | **HEAD** /studio/v2/workflows/{workflow_id}/results | Get workflow results
[**studio_v2_workflows_workflow_id_results_put**](WorkflowsApi.md#studio_v2_workflows_workflow_id_results_put) | **PUT** /studio/v2/workflows/{workflow_id}/results | Overwrite workflow results
[**studio_v2_workflows_workflow_id_run_results_get**](WorkflowsApi.md#studio_v2_workflows_workflow_id_run_results_get) | **GET** /studio/v2/workflows/{workflow_id}/run-results | Get workflow results configuration
[**studio_v2_workflows_workflow_id_run_results_head**](WorkflowsApi.md#studio_v2_workflows_workflow_id_run_results_head) | **HEAD** /studio/v2/workflows/{workflow_id}/run-results | Get workflow results configuration
[**studio_workflows_get**](WorkflowsApi.md#studio_workflows_get) | **GET** /studio/workflows | List all workflows
[**studio_workflows_import_post**](WorkflowsApi.md#studio_workflows_import_post) | **POST** /studio/workflows/import | Import workflow
[**studio_workflows_post**](WorkflowsApi.md#studio_workflows_post) | **POST** /studio/workflows | Create a workflows
[**studio_workflows_schema_get_get**](WorkflowsApi.md#studio_workflows_schema_get_get) | **GET** /studio/workflows/schema/get | Get JSON workflow schema
[**studio_workflows_schema_validate_post**](WorkflowsApi.md#studio_workflows_schema_validate_post) | **POST** /studio/workflows/schema/validate | Check if a given workflow schema is valid
[**studio_workflows_workflow_id_delete**](WorkflowsApi.md#studio_workflows_workflow_id_delete) | **DELETE** /studio/workflows/{workflow_id} | Delete workflow
[**studio_workflows_workflow_id_deploy_post**](WorkflowsApi.md#studio_workflows_workflow_id_deploy_post) | **POST** /studio/workflows/{workflow_id}/deploy | Deploy workflow
[**studio_workflows_workflow_id_export_get**](WorkflowsApi.md#studio_workflows_workflow_id_export_get) | **GET** /studio/workflows/{workflow_id}/export | Export a workflow
[**studio_workflows_workflow_id_export_head**](WorkflowsApi.md#studio_workflows_workflow_id_export_head) | **HEAD** /studio/workflows/{workflow_id}/export | Export a workflow
[**studio_workflows_workflow_id_get**](WorkflowsApi.md#studio_workflows_workflow_id_get) | **GET** /studio/workflows/{workflow_id} | Get one workflow
[**studio_workflows_workflow_id_put**](WorkflowsApi.md#studio_workflows_workflow_id_put) | **PUT** /studio/workflows/{workflow_id} | Update workflow
[**studio_workflows_workflow_id_stop_post**](WorkflowsApi.md#studio_workflows_workflow_id_stop_post) | **POST** /studio/workflows/{workflow_id}/stop | Stop workflow deployment


# **runner_portals_workflow_definition_id_get**
> RunnerPortal runner_portals_workflow_definition_id_get(workflow_definition_id)

Get a portal

Get a portal from a workflow definition id (the id used in the studio).

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.runner_error import RunnerError
from ryax_sdk.model.runner_portal import RunnerPortal
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_definition_id = "workflow_definition_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get a portal
        api_response = api_instance.runner_portals_workflow_definition_id_get(workflow_definition_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->runner_portals_workflow_definition_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_definition_id** | **str**|  |

### Return type

[**RunnerPortal**](RunnerPortal.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Portal fetched successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_endpoint_get**
> StudioWorkflowEndpoint studio_v2_workflows_workflow_id_endpoint_get(workflow_id)

Get workflow endpoint

Get the workflow endpoint if this workflow support it

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_endpoint import StudioWorkflowEndpoint
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get workflow endpoint
        api_response = api_instance.studio_v2_workflows_workflow_id_endpoint_get(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_endpoint_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**StudioWorkflowEndpoint**](StudioWorkflowEndpoint.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | This workflow does not support endpoint |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_endpoint_head**
> StudioWorkflowEndpoint studio_v2_workflows_workflow_id_endpoint_head(workflow_id)

Get workflow endpoint

Get the workflow endpoint if this workflow support it

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_endpoint import StudioWorkflowEndpoint
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get workflow endpoint
        api_response = api_instance.studio_v2_workflows_workflow_id_endpoint_head(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_endpoint_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**StudioWorkflowEndpoint**](StudioWorkflowEndpoint.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | This workflow does not support endpoint |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_endpoint_put**
> StudioWorkflowEndpoint studio_v2_workflows_workflow_id_endpoint_put(workflow_id)

Update workflow endpoint

Update workflow endpoint

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_endpoint import StudioWorkflowEndpoint
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    body = StudioWorkflowEndpoint(
        path="/my/custom/path",
    ) # StudioWorkflowEndpoint |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update workflow endpoint
        api_response = api_instance.studio_v2_workflows_workflow_id_endpoint_put(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_endpoint_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update workflow endpoint
        api_response = api_instance.studio_v2_workflows_workflow_id_endpoint_put(workflow_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_endpoint_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **body** | [**StudioWorkflowEndpoint**](StudioWorkflowEndpoint.md)|  | [optional]

### Return type

[**StudioWorkflowEndpoint**](StudioWorkflowEndpoint.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Workflow endpoint could not be updated |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_get**
> StudioWorkflowExtended studio_v2_workflows_workflow_id_get(workflow_id)

Get one workflow with details

Get the requested workflow by ID with all details

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_extended import StudioWorkflowExtended
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one workflow with details
        api_response = api_instance.studio_v2_workflows_workflow_id_get(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**StudioWorkflowExtended**](StudioWorkflowExtended.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow fetched successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_links_put**
> studio_v2_workflows_workflow_id_links_put(workflow_id)

Update workflow links

For each given action, redo all its links.

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_update_workflow_modules_links import StudioUpdateWorkflowModulesLinks
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    body = StudioUpdateWorkflowModulesLinks(
        links=[
            StudioUpdateWorkflowModuleLinks(
                next_modules_ids=[3ff80197-c975-4f20-a0ca-c35bda9f092b],
                module_id="3ff80197-c975-4f20-a0ca-c35bda9f092b",
            ),
        ],
    ) # StudioUpdateWorkflowModulesLinks |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update workflow links
        api_instance.studio_v2_workflows_workflow_id_links_put(workflow_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_links_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update workflow links
        api_instance.studio_v2_workflows_workflow_id_links_put(workflow_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_links_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **body** | [**StudioUpdateWorkflowModulesLinks**](StudioUpdateWorkflowModulesLinks.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow links updated successfully |  -  |
**400** | Workflow not updatable |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_results_get**
> [StudioWorkflowResult] studio_v2_workflows_workflow_id_results_get(workflow_id)

Get workflow results

Get all workflow results

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_workflow_result import StudioWorkflowResult
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get workflow results
        api_response = api_instance.studio_v2_workflows_workflow_id_results_get(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_results_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**[StudioWorkflowResult]**](StudioWorkflowResult.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Workflow result get error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_results_head**
> [StudioWorkflowResult] studio_v2_workflows_workflow_id_results_head(workflow_id)

Get workflow results

Get all workflow results

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_workflow_result import StudioWorkflowResult
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get workflow results
        api_response = api_instance.studio_v2_workflows_workflow_id_results_head(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_results_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**[StudioWorkflowResult]**](StudioWorkflowResult.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Workflow result get error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_results_put**
> [StudioAddWorkflowResultResponse] studio_v2_workflows_workflow_id_results_put(workflow_id)

Overwrite workflow results

Overwrite workflow results

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_add_workflow_result_response import StudioAddWorkflowResultResponse
from ryax_sdk.model.studio_add_many_workflow_result_request import StudioAddManyWorkflowResultRequest
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    body = StudioAddManyWorkflowResultRequest(
        workflow_results_to_add=[
            StudioWorkflowResult(
                key="Result Value",
                description="Value from input "My input name" of action "My action name"",
                type={},
                workflow_module_io_id="3ff80197-c975-4f20-a0ca-c35bda9f092b",
            ),
        ],
    ) # StudioAddManyWorkflowResultRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Overwrite workflow results
        api_response = api_instance.studio_v2_workflows_workflow_id_results_put(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_results_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Overwrite workflow results
        api_response = api_instance.studio_v2_workflows_workflow_id_results_put(workflow_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_results_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **body** | [**StudioAddManyWorkflowResultRequest**](StudioAddManyWorkflowResultRequest.md)|  | [optional]

### Return type

[**[StudioAddWorkflowResultResponse]**](StudioAddWorkflowResultResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Workflow result add error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_run_results_get**
> StudioWorkflowResultConfiguration studio_v2_workflows_workflow_id_run_results_get(workflow_id)

Get workflow results configuration

Get all workflow results configuration

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_result_configuration import StudioWorkflowResultConfiguration
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get workflow results configuration
        api_response = api_instance.studio_v2_workflows_workflow_id_run_results_get(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_run_results_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**StudioWorkflowResultConfiguration**](StudioWorkflowResultConfiguration.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Error while getting workflow results configuration |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_v2_workflows_workflow_id_run_results_head**
> StudioWorkflowResultConfiguration studio_v2_workflows_workflow_id_run_results_head(workflow_id)

Get workflow results configuration

Get all workflow results configuration

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_result_configuration import StudioWorkflowResultConfiguration
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get workflow results configuration
        api_response = api_instance.studio_v2_workflows_workflow_id_run_results_head(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_v2_workflows_workflow_id_run_results_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**StudioWorkflowResultConfiguration**](StudioWorkflowResultConfiguration.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**400** | Error while getting workflow results configuration |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_get**
> [StudioWorkflow] studio_workflows_get()

List all workflows

List all workflows available

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_workflow import StudioWorkflow
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    search = "search_example" # str |  (optional)
    deployment_status = "deployment_status_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # List all workflows
        api_response = api_instance.studio_workflows_get(search=search, deployment_status=deployment_status)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **str**|  | [optional]
 **deployment_status** | **str**|  | [optional]

### Return type

[**[StudioWorkflow]**](StudioWorkflow.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflows fetched successfully |  -  |
**400** | Deployment status filter is incorrect |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_import_post**
> StudioAddWorkflowResponse studio_workflows_import_post(workflow_packaging_file)

Import workflow

Import workflow packaging file

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_add_workflow_response import StudioAddWorkflowResponse
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_packaging_file = open('/path/to/file', 'rb') # file_type | The workflow packaging file to upload.

    # example passing only required values which don't have defaults set
    try:
        # Import workflow
        api_response = api_instance.studio_workflows_import_post(workflow_packaging_file)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_import_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_packaging_file** | **file_type**| The workflow packaging file to upload. |

### Return type

[**StudioAddWorkflowResponse**](StudioAddWorkflowResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Workflow imported successfully |  -  |
**400** | Error when importing the workflow |  -  |
**404** | Action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_post**
> StudioAddWorkflowResponse studio_workflows_post()

Create a workflows

Create a new workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_add_workflow_response import StudioAddWorkflowResponse
from ryax_sdk.model.studio_add_workflow_request import StudioAddWorkflowRequest
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    body = StudioAddWorkflowRequest(
        description="Detect object in videos in real time",
        name="Video detection",
        from_workflow="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    ) # StudioAddWorkflowRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a workflows
        api_response = api_instance.studio_workflows_post(body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StudioAddWorkflowRequest**](StudioAddWorkflowRequest.md)|  | [optional]

### Return type

[**StudioAddWorkflowResponse**](StudioAddWorkflowResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Workflow created successfully |  -  |
**401** | Authentication failed |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_schema_get_get**
> studio_workflows_schema_get_get()

Get JSON workflow schema

Get the JSON workflow schema that is used to validate workflows

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Get JSON workflow schema
        api_instance.studio_workflows_schema_get_get()
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_schema_get_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow schema fetched successfully. |  -  |
**404** | Workflow schema not found. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_schema_validate_post**
> StudioWorkflowValidation studio_workflows_schema_validate_post(workflow_schema_file)

Check if a given workflow schema is valid

Check the validity of a given workflow specification file

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_validation import StudioWorkflowValidation
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_schema_file = open('/path/to/file', 'rb') # file_type | The workflow schema file to check.

    # example passing only required values which don't have defaults set
    try:
        # Check if a given workflow schema is valid
        api_response = api_instance.studio_workflows_schema_validate_post(workflow_schema_file)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_schema_validate_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_schema_file** | **file_type**| The workflow schema file to check. |

### Return type

[**StudioWorkflowValidation**](StudioWorkflowValidation.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow schema checked. |  -  |
**400** | Workflow schema validation failed. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_delete**
> studio_workflows_workflow_id_delete(workflow_id)

Delete workflow

Delete a workflow by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete workflow
        api_instance.studio_workflows_workflow_id_delete(workflow_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_workflow_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow deleted successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_deploy_post**
> studio_workflows_workflow_id_deploy_post(workflow_id)

Deploy workflow

Deploy a workflow by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Deploy workflow
        api_instance.studio_workflows_workflow_id_deploy_post(workflow_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_workflow_id_deploy_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow deploying triggered |  -  |
**400** | Workflow not valid |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_export_get**
> file_type studio_workflows_workflow_id_export_get(workflow_id)

Export a workflow

Export the given workflow zip file by id

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Export a workflow
        api_response = api_instance.studio_workflows_workflow_id_export_get(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_workflow_id_export_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

**file_type**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/zip


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow exported successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_export_head**
> file_type studio_workflows_workflow_id_export_head(workflow_id)

Export a workflow

Export the given workflow zip file by id

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Export a workflow
        api_response = api_instance.studio_workflows_workflow_id_export_head(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_workflow_id_export_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

**file_type**

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/zip


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow exported successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_get**
> StudioWorkflowDetails studio_workflows_workflow_id_get(workflow_id)

Get one workflow

Get the requested workflow by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_details import StudioWorkflowDetails
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one workflow
        api_response = api_instance.studio_workflows_workflow_id_get(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_workflow_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**StudioWorkflowDetails**](StudioWorkflowDetails.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow fetched successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_put**
> studio_workflows_workflow_id_put(workflow_id)

Update workflow

Update the data of a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_update_workflow_request import StudioUpdateWorkflowRequest
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    body = StudioUpdateWorkflowRequest(
        description="Detect object in videos in real time",
        name="Video detection",
    ) # StudioUpdateWorkflowRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update workflow
        api_instance.studio_workflows_workflow_id_put(workflow_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_workflow_id_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update workflow
        api_instance.studio_workflows_workflow_id_put(workflow_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_workflow_id_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **body** | [**StudioUpdateWorkflowRequest**](StudioUpdateWorkflowRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow updated successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_stop_post**
> studio_workflows_workflow_id_stop_post(workflow_id)

Stop workflow deployment

Stop a workflow deployment by workflow ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflows_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflows_api.WorkflowsApi(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Stop workflow deployment
        api_instance.studio_workflows_workflow_id_stop_post(workflow_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowsApi->studio_workflows_workflow_id_stop_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Stop workflow deployment triggered |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

