# StudioUpdateWorkflowModuleInputValueWithId


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Input id | 
**static_value** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | Static value to set for the input | [optional] 
**project_variable_value** | **str, none_type** | Project Variable id to set for the input | [optional] 
**reference_value** | **str, none_type** | Referencce id to set for the input | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


