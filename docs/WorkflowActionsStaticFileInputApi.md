# ryax_sdk.WorkflowActionsStaticFileInputApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post**](WorkflowActionsStaticFileInputApi.md#studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post) | **POST** /studio/workflows/{workflow_id}/modules/{module_id}/inputs/{workflow_input_id}/file | Update workflow action input with static file


# **studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post**
> studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post(workflow_id, module_id, workflow_input_id, file)

Update workflow action input with static file

Update the value of action input in a workflow wihth a static file

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_static_file_input_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_static_file_input_api.WorkflowActionsStaticFileInputApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    module_id = "module_id_example" # str | 
    workflow_input_id = "workflow_input_id_example" # str | 
    file = open('/path/to/file', 'rb') # file_type | The file to use as input.

    # example passing only required values which don't have defaults set
    try:
        # Update workflow action input with static file
        api_instance.studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post(workflow_id, module_id, workflow_input_id, file)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsStaticFileInputApi->studio_workflows_workflow_id_modules_module_id_inputs_workflow_input_id_file_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **module_id** | **str**|  |
 **workflow_input_id** | **str**|  |
 **file** | **file_type**| The file to use as input. |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Static file uploaded successfully |  -  |
**400** | Error while importing the selected file |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

