# ryax_sdk.RepositoriesApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**repository_sources_get**](RepositoriesApi.md#repository_sources_get) | **GET** /repository/sources | List all repositories v1
[**repository_sources_post**](RepositoriesApi.md#repository_sources_post) | **POST** /repository/sources | Add git_repo
[**repository_sources_source_id_delete**](RepositoriesApi.md#repository_sources_source_id_delete) | **DELETE** /repository/sources/{source_id} | Delete git_repo
[**repository_sources_source_id_get**](RepositoriesApi.md#repository_sources_source_id_get) | **GET** /repository/sources/{source_id} | Get one git_repo (v1)
[**repository_sources_source_id_put**](RepositoriesApi.md#repository_sources_source_id_put) | **PUT** /repository/sources/{source_id} | Update git_repo
[**repository_sources_source_id_scan_post**](RepositoriesApi.md#repository_sources_source_id_scan_post) | **POST** /repository/sources/{source_id}/scan | Scan git_repo
[**repository_v2_sources_get**](RepositoriesApi.md#repository_v2_sources_get) | **GET** /repository/v2/sources | List all git repos
[**repository_v2_sources_source_id_build_post**](RepositoriesApi.md#repository_v2_sources_source_id_build_post) | **POST** /repository/v2/sources/{source_id}/build | Build all actions from last scan in this git repo
[**repository_v2_sources_source_id_get**](RepositoriesApi.md#repository_v2_sources_source_id_get) | **GET** /repository/v2/sources/{source_id} | Get one git_repo
[**repository_v2_sources_source_id_scan_post**](RepositoriesApi.md#repository_v2_sources_source_id_scan_post) | **POST** /repository/v2/sources/{source_id}/scan | Scan git repo


# **repository_sources_get**
> [RepositoryGitRepoSchemaV1] repository_sources_get()

List all repositories v1

List all code repositories available

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_git_repo_schema_v1 import RepositoryGitRepoSchemaV1
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List all repositories v1
        api_response = api_instance.repository_sources_get()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[RepositoryGitRepoSchemaV1]**](RepositoryGitRepoSchemaV1.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Repositories fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_sources_post**
> RepositoryGitRepoSchemaV1 repository_sources_post()

Add git_repo

Add new user git_repo

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_git_repo_schema_v1 import RepositoryGitRepoSchemaV1
from ryax_sdk.model.repository_add_git_repo import RepositoryAddGitRepo
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)
    body = RepositoryAddGitRepo(
        name="Gitlab Repository",
        url="http://blbl.fr",
        username="User1",
        password="4_r3411y_g00d_p455w0rd!",
    ) # RepositoryAddGitRepo |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Add git_repo
        api_response = api_instance.repository_sources_post(body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RepositoryAddGitRepo**](RepositoryAddGitRepo.md)|  | [optional]

### Return type

[**RepositoryGitRepoSchemaV1**](RepositoryGitRepoSchemaV1.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Repository created successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_sources_source_id_delete**
> repository_sources_source_id_delete(source_id)

Delete git_repo

Delete a git_repo

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)
    source_id = "source_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete git_repo
        api_instance.repository_sources_source_id_delete(source_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_source_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Repository deleted successfully |  -  |
**404** | Repository not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_sources_source_id_get**
> RepositoryGitRepoDetailsSchemaV1 repository_sources_source_id_get(source_id)

Get one git_repo (v1)

Get the requested git_repo by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_git_repo_details_schema_v1 import RepositoryGitRepoDetailsSchemaV1
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)
    source_id = "source_id_example" # str | 
    module_sorting = "module_sorting_example" # str |  (optional)
    module_sorting_type = "module_sorting_type_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get one git_repo (v1)
        api_response = api_instance.repository_sources_source_id_get(source_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_source_id_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get one git_repo (v1)
        api_response = api_instance.repository_sources_source_id_get(source_id, module_sorting=module_sorting, module_sorting_type=module_sorting_type)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_source_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_id** | **str**|  |
 **module_sorting** | **str**|  | [optional]
 **module_sorting_type** | **str**|  | [optional]

### Return type

[**RepositoryGitRepoDetailsSchemaV1**](RepositoryGitRepoDetailsSchemaV1.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Repository fetched successfully |  -  |
**404** | Repository not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_sources_source_id_put**
> RepositoryGitRepoSchemaV1 repository_sources_source_id_put(source_id)

Update git_repo

Update the data of one git_repo

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_update_git_repo import RepositoryUpdateGitRepo
from ryax_sdk.model.repository_git_repo_schema_v1 import RepositoryGitRepoSchemaV1
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)
    source_id = "source_id_example" # str | 
    body = RepositoryUpdateGitRepo(
        name="Gitlab Repository",
        url="http://github.com/project",
        username="User1",
        password="4_r3411y_g00d_p455w0rd!",
    ) # RepositoryUpdateGitRepo |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update git_repo
        api_response = api_instance.repository_sources_source_id_put(source_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_source_id_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update git_repo
        api_response = api_instance.repository_sources_source_id_put(source_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_source_id_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_id** | **str**|  |
 **body** | [**RepositoryUpdateGitRepo**](RepositoryUpdateGitRepo.md)|  | [optional]

### Return type

[**RepositoryGitRepoSchemaV1**](RepositoryGitRepoSchemaV1.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Repository updated successfully |  -  |
**404** | Repository not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_sources_source_id_scan_post**
> RepositoryRepositoryScan repository_sources_source_id_scan_post(source_id)

Scan git_repo

Launch git_repo analysis (scan method)

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_repository_scan import RepositoryRepositoryScan
from ryax_sdk.model.repository_git_repo_scan_request import RepositoryGitRepoScanRequest
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)
    source_id = "source_id_example" # str | 
    body = RepositoryGitRepoScanRequest(
        branch="release-1",
    ) # RepositoryGitRepoScanRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Scan git_repo
        api_response = api_instance.repository_sources_source_id_scan_post(source_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_source_id_scan_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Scan git_repo
        api_response = api_instance.repository_sources_source_id_scan_post(source_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_sources_source_id_scan_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_id** | **str**|  |
 **body** | [**RepositoryGitRepoScanRequest**](RepositoryGitRepoScanRequest.md)|  | [optional]

### Return type

[**RepositoryRepositoryScan**](RepositoryRepositoryScan.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Repository scanned successfully |  -  |
**404** | Repository not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_v2_sources_get**
> [RepositoryGitRepoPreview] repository_v2_sources_get()

List all git repos

List all code repositories available

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_git_repo_preview import RepositoryGitRepoPreview
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List all git repos
        api_response = api_instance.repository_v2_sources_get()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_v2_sources_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[RepositoryGitRepoPreview]**](RepositoryGitRepoPreview.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Repositories fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_v2_sources_source_id_build_post**
> [RepositoryActionBuild] repository_v2_sources_source_id_build_post(source_id)

Build all actions from last scan in this git repo

Build all actions in this git repo

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_action_build import RepositoryActionBuild
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)
    source_id = "source_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Build all actions from last scan in this git repo
        api_response = api_instance.repository_v2_sources_source_id_build_post(source_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_v2_sources_source_id_build_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_id** | **str**|  |

### Return type

[**[RepositoryActionBuild]**](RepositoryActionBuild.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Actions built successfully |  -  |
**400** | unauthorised action build |  -  |
**404** | Entity not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_v2_sources_source_id_get**
> RepositoryGitRepo repository_v2_sources_source_id_get(source_id)

Get one git_repo

Get the requested git_repo by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_git_repo import RepositoryGitRepo
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)
    source_id = "source_id_example" # str | 
    action_sorting = "action_sorting_example" # str |  (optional)
    action_sorting_type = "action_sorting_type_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get one git_repo
        api_response = api_instance.repository_v2_sources_source_id_get(source_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_v2_sources_source_id_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get one git_repo
        api_response = api_instance.repository_v2_sources_source_id_get(source_id, action_sorting=action_sorting, action_sorting_type=action_sorting_type)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_v2_sources_source_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_id** | **str**|  |
 **action_sorting** | **str**|  | [optional]
 **action_sorting_type** | **str**|  | [optional]

### Return type

[**RepositoryGitRepo**](RepositoryGitRepo.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Repository fetched successfully |  -  |
**404** | Repository not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_v2_sources_source_id_scan_post**
> RepositoryGitRepo repository_v2_sources_source_id_scan_post(source_id)

Scan git repo

Launch git_repo analysis (scan method) and return the same data as GET /v2/sources/{source_id}

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repositories_api
from ryax_sdk.model.repository_git_repo import RepositoryGitRepo
from ryax_sdk.model.repository_git_repo_scan_request import RepositoryGitRepoScanRequest
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repositories_api.RepositoriesApi(api_client)
    source_id = "source_id_example" # str | 
    body = RepositoryGitRepoScanRequest(
        branch="release-1",
    ) # RepositoryGitRepoScanRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Scan git repo
        api_response = api_instance.repository_v2_sources_source_id_scan_post(source_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_v2_sources_source_id_scan_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Scan git repo
        api_response = api_instance.repository_v2_sources_source_id_scan_post(source_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoriesApi->repository_v2_sources_source_id_scan_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_id** | **str**|  |
 **body** | [**RepositoryGitRepoScanRequest**](RepositoryGitRepoScanRequest.md)|  | [optional]

### Return type

[**RepositoryGitRepo**](RepositoryGitRepo.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Repository scanned successfully |  -  |
**400** | Repository access is not valid, unable to fetch |  -  |
**404** | Repository not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

