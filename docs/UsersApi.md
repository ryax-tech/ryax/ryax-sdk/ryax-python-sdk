# ryax_sdk.UsersApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorization_users_get**](UsersApi.md#authorization_users_get) | **GET** /authorization/users | List users
[**authorization_users_post**](UsersApi.md#authorization_users_post) | **POST** /authorization/users | Create a user
[**authorization_users_user_id_delete**](UsersApi.md#authorization_users_user_id_delete) | **DELETE** /authorization/users/{user_id} | Delete user
[**authorization_users_user_id_get**](UsersApi.md#authorization_users_user_id_get) | **GET** /authorization/users/{user_id} | Get one user
[**authorization_users_user_id_put**](UsersApi.md#authorization_users_user_id_put) | **PUT** /authorization/users/{user_id} | Update a user


# **authorization_users_get**
> [AuthorizationUserDetails] authorization_users_get()

List users

Show all users

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import users_api
from ryax_sdk.model.authorization_user_details import AuthorizationUserDetails
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List users
        api_response = api_instance.authorization_users_get()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UsersApi->authorization_users_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[AuthorizationUserDetails]**](AuthorizationUserDetails.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Users list fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_users_post**
> AuthorizationAddUserResponse authorization_users_post()

Create a user

Create a new user

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import users_api
from ryax_sdk.model.authorization_add_user_request import AuthorizationAddUserRequest
from ryax_sdk.model.authorization_add_user_response import AuthorizationAddUserResponse
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    body = AuthorizationAddUserRequest(
        email="user1@example.com",
        role="User",
        password="secret_password",
        username="user_1",
        comment="Testing user for project 1",
    ) # AuthorizationAddUserRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a user
        api_response = api_instance.authorization_users_post(body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UsersApi->authorization_users_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AuthorizationAddUserRequest**](AuthorizationAddUserRequest.md)|  | [optional]

### Return type

[**AuthorizationAddUserResponse**](AuthorizationAddUserResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | User created successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_users_user_id_delete**
> authorization_users_user_id_delete(user_id)

Delete user

Delete a user with ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import users_api
from ryax_sdk.model.authorization_error import AuthorizationError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    user_id = "user_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete user
        api_instance.authorization_users_user_id_delete(user_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UsersApi->authorization_users_user_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Users list fetched successfully |  -  |
**400** | Impossible to delete yourself. |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_users_user_id_get**
> AuthorizationUserDetails authorization_users_user_id_get(user_id)

Get one user

Get the requested user by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import users_api
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_user_details import AuthorizationUserDetails
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    user_id = "user_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one user
        api_response = api_instance.authorization_users_user_id_get(user_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UsersApi->authorization_users_user_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  |

### Return type

[**AuthorizationUserDetails**](AuthorizationUserDetails.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User fetched successfully |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_users_user_id_put**
> AuthorizationUserDetails authorization_users_user_id_put(user_id)

Update a user

Update user data

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import users_api
from ryax_sdk.model.authorization_update_user_request import AuthorizationUpdateUserRequest
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_user_details import AuthorizationUserDetails
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = users_api.UsersApi(api_client)
    user_id = "user_id_example" # str | 
    body = AuthorizationUpdateUserRequest(
        email="user1@example.com",
        role="User",
        password="secret_password",
        username="user_1",
        comment="Testing user for project 1",
    ) # AuthorizationUpdateUserRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update a user
        api_response = api_instance.authorization_users_user_id_put(user_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UsersApi->authorization_users_user_id_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update a user
        api_response = api_instance.authorization_users_user_id_put(user_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UsersApi->authorization_users_user_id_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  |
 **body** | [**AuthorizationUpdateUserRequest**](AuthorizationUpdateUserRequest.md)|  | [optional]

### Return type

[**AuthorizationUserDetails**](AuthorizationUserDetails.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User updated successfully |  -  |
**400** | Cannot perform this operation, otherwise Ryax will not have any administrators |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

