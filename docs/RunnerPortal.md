# RunnerPortal


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**workflow_deployment_id** | **str** | ID of the workflow | 
**workflow_definition_id** | **str** | ID of the workflow | 
**name** | **str** | Name of the workflow | 
**outputs** | [**[RunnerModuleIO]**](RunnerModuleIO.md) | Action outputs | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


