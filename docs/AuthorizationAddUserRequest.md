# AuthorizationAddUserRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | User&#39;s email address | 
**role** | **str** | User&#39;s role | 
**password** | **str** | User&#39;s password | 
**username** | **str** | Username | 
**comment** | **str** | A comment on the user | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


