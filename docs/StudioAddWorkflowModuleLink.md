# StudioAddWorkflowModuleLink


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**input_module_id** | **str** | Identifier of the input workflow action | 
**output_module_id** | **str** | Identifier of the output workflow action | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


