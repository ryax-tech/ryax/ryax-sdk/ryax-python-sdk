# ryax_sdk.ExecutionsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**runner_portals_workflow_definition_id_post**](ExecutionsApi.md#runner_portals_workflow_definition_id_post) | **POST** /runner/portals/{workflow_definition_id} | Trigger a new workflow run
[**runner_workflow_runs_get**](ExecutionsApi.md#runner_workflow_runs_get) | **GET** /runner/workflow_runs | List workflow runs
[**runner_workflow_runs_workflow_run_id_delete**](ExecutionsApi.md#runner_workflow_runs_workflow_run_id_delete) | **DELETE** /runner/workflow_runs/{workflow_run_id} | Delete a workflow run
[**runner_workflow_runs_workflow_run_id_get**](ExecutionsApi.md#runner_workflow_runs_workflow_run_id_get) | **GET** /runner/workflow_runs/{workflow_run_id} | Get a workflow run


# **runner_portals_workflow_definition_id_post**
> RunnerWorkflowRunTriggerResponse runner_portals_workflow_definition_id_post(workflow_definition_id)

Trigger a new workflow run

Await a multipart, where each part is named after an input and contains the data.

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import executions_api
from ryax_sdk.model.runner_error import RunnerError
from ryax_sdk.model.runner_workflow_run_trigger_response import RunnerWorkflowRunTriggerResponse
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = executions_api.ExecutionsApi(api_client)
    workflow_definition_id = "workflow_definition_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Trigger a new workflow run
        api_response = api_instance.runner_portals_workflow_definition_id_post(workflow_definition_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ExecutionsApi->runner_portals_workflow_definition_id_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_definition_id** | **str**|  |

### Return type

[**RunnerWorkflowRunTriggerResponse**](RunnerWorkflowRunTriggerResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Workflow run created successfully |  -  |
**400** | Malformed inputs |  -  |
**404** | Action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_workflow_runs_get**
> [RunnerWorkflowRunSchemaMinimal] runner_workflow_runs_get()

List workflow runs

List workflow runs.

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import executions_api
from ryax_sdk.model.runner_error import RunnerError
from ryax_sdk.model.runner_workflow_run_schema_minimal import RunnerWorkflowRunSchemaMinimal
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = executions_api.ExecutionsApi(api_client)
    workflow_id = "workflow_id_example" # str |  (optional)
    range = "range_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # List workflow runs
        api_response = api_instance.runner_workflow_runs_get(workflow_id=workflow_id, range=range)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ExecutionsApi->runner_workflow_runs_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | [optional]
 **range** | **str**|  | [optional]

### Return type

[**[RunnerWorkflowRunSchemaMinimal]**](RunnerWorkflowRunSchemaMinimal.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow runs fetched successfully |  -  |
**400** | Too many execution found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_workflow_runs_workflow_run_id_delete**
> runner_workflow_runs_workflow_run_id_delete(workflow_run_id)

Delete a workflow run

Delete a workflow run by its id. Deletes all execution results and connections belonging to that workflow run.

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import executions_api
from ryax_sdk.model.runner_error import RunnerError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = executions_api.ExecutionsApi(api_client)
    workflow_run_id = "workflow_run_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete a workflow run
        api_instance.runner_workflow_runs_workflow_run_id_delete(workflow_run_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ExecutionsApi->runner_workflow_runs_workflow_run_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_run_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow run deleted successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_workflow_runs_workflow_run_id_get**
> RunnerWorkflowRun runner_workflow_runs_workflow_run_id_get(workflow_run_id)

Get a workflow run

Get one complete workflow run from its id.

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import executions_api
from ryax_sdk.model.runner_error import RunnerError
from ryax_sdk.model.runner_workflow_run import RunnerWorkflowRun
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = executions_api.ExecutionsApi(api_client)
    workflow_run_id = "workflow_run_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get a workflow run
        api_response = api_instance.runner_workflow_runs_workflow_run_id_get(workflow_run_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ExecutionsApi->runner_workflow_runs_workflow_run_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_run_id** | **str**|  |

### Return type

[**RunnerWorkflowRun**](RunnerWorkflowRun.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Portal fetched successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

