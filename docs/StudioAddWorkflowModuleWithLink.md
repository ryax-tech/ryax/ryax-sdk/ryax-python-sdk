# StudioAddWorkflowModuleWithLink


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**module_definition_id** | **str** | Id of the action | 
**replace_workflow_module_id** | **str** | Id of the workflow action to replace | [optional] 
**parent_workflow_module_id** | **str** | Id of the workflow action | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


