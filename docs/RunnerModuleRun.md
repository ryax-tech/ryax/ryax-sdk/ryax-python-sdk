# RunnerModuleRun


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str, none_type** | ID of the action execution | [optional] 
**state** | **str** | Action Execution state: [&#39;Success&#39;, &#39;Error&#39;, &#39;Running&#39;, &#39;Waiting&#39;, &#39;Canceled&#39;] | [optional] 
**module** | [**RunnerModule**](RunnerModule.md) |  | [optional] 
**submitted_at_date** | **datetime, none_type** | execution submission datetime | [optional] 
**started_at_date** | **datetime, none_type** | execution submission datetime | [optional] 
**ended_at_date** | **datetime, none_type** | execution submission datetime | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


