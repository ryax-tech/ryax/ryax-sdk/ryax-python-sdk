# ryax_sdk.WorkflowActionsInputsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_workflows_workflow_id_modules_workflow_module_id_inputs_get**](WorkflowActionsInputsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_inputs_get) | **GET** /studio/workflows/{workflow_id}/modules/{workflow_module_id}/inputs | List all workflows action inputs
[**studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_file_delete**](WorkflowActionsInputsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_file_delete) | **DELETE** /studio/workflows/{workflow_id}/modules/{workflow_module_id}/inputs/{workflow_input_id}/file | Delete workflow action input file
[**studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put**](WorkflowActionsInputsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put) | **PUT** /studio/workflows/{workflow_id}/modules/{workflow_module_id}/inputs/{workflow_input_id}/value | Update workflow action input


# **studio_workflows_workflow_id_modules_workflow_module_id_inputs_get**
> [StudioWorkflowModuleInput] studio_workflows_workflow_id_modules_workflow_module_id_inputs_get(workflow_id, workflow_module_id)

List all workflows action inputs

List all inputs available in a workflow action

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_inputs_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_module_input import StudioWorkflowModuleInput
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_inputs_api.WorkflowActionsInputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # List all workflows action inputs
        api_response = api_instance.studio_workflows_workflow_id_modules_workflow_module_id_inputs_get(workflow_id, workflow_module_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsInputsApi->studio_workflows_workflow_id_modules_workflow_module_id_inputs_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |

### Return type

[**[StudioWorkflowModuleInput]**](StudioWorkflowModuleInput.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow action inputs fetched successfully |  -  |
**404** | Workflow action inputs not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_file_delete**
> studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_file_delete(workflow_id, workflow_module_id, workflow_input_id)

Delete workflow action input file

Delete the static file of action input in a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_inputs_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_inputs_api.WorkflowActionsInputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 
    workflow_input_id = "workflow_input_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete workflow action input file
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_file_delete(workflow_id, workflow_module_id, workflow_input_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsInputsApi->studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_file_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |
 **workflow_input_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow input file deleted successfully |  -  |
**400** | Workflow not updatable |  -  |
**404** | Workflow or action or input or file not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put**
> studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put(workflow_id, workflow_module_id, workflow_input_id)

Update workflow action input

Update the value of action input in a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_inputs_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_update_workflow_module_input_value import StudioUpdateWorkflowModuleInputValue
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_inputs_api.WorkflowActionsInputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 
    workflow_input_id = "workflow_input_id_example" # str | 
    body = StudioUpdateWorkflowModuleInputValue(
        static_value={},
        reference_value="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    ) # StudioUpdateWorkflowModuleInputValue |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update workflow action input
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put(workflow_id, workflow_module_id, workflow_input_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsInputsApi->studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update workflow action input
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put(workflow_id, workflow_module_id, workflow_input_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsInputsApi->studio_workflows_workflow_id_modules_workflow_module_id_inputs_workflow_input_id_value_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |
 **workflow_input_id** | **str**|  |
 **body** | [**StudioUpdateWorkflowModuleInputValue**](StudioUpdateWorkflowModuleInputValue.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow input updated successfully |  -  |
**404** | Workflow or action or input not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

