# RepositoryScanResultBar


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scan_date** | **datetime** | Source last scan date | [optional] 
**bar** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | Numbers of last scan modules arranged by status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


