# RunnerModule


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**technical_name** | **str** | Technical name of the action | 
**container_image** | **str** | container image full name | 
**kind** | **str** | Action kind: [&#39;Source&#39;, &#39;Processor&#39;, &#39;Publisher&#39;] | 
**version** | **str** | action version | 
**name** | **str** | Human name of the action. | [optional] 
**outputs** | [**[RunnerModuleIO]**](RunnerModuleIO.md) | Action outputs | [optional] 
**id** | **str** |  | [optional] 
**description** | **str** | Description of the action. | [optional] 
**inputs** | [**[RunnerModuleIO]**](RunnerModuleIO.md) | Actions inputs | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


