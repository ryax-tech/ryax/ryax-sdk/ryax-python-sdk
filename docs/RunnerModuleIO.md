# RunnerModuleIO


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Action input type: [&#39;NONE&#39;, &#39;BYTES&#39;, &#39;STRING&#39;, &#39;INTEGER&#39;, &#39;FLOAT&#39;, &#39;BOOLEAN&#39;, &#39;FILE&#39;, &#39;DIRECTORY&#39;, &#39;FILE_REFERENCE&#39;, &#39;DIRECTORY_REFERENCE&#39;, &#39;LONGSTRING&#39;, &#39;PASSWORD&#39;, &#39;ENUM&#39;, &#39;TABLE&#39;] | 
**id** | **str** | ID of the workflow_deployment action input | 
**display_name** | **str** | This string is used to display the IO to the user | 
**enum_values** | **[str]** | The list of values that this IO can take if it is an enum. | [optional] 
**value** | **str, none_type** | Action outputs value | [optional] 
**help** | **str** | This string helps the user understand the IO | [optional] 
**headers** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | Headers to use for the file download | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


