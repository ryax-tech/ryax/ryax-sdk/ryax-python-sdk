# StudioAddWorkflowModuleDynamicOutput


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **str** | Output name to display on Webui | 
**help** | **str** | Output help text | 
**type** | **str** | Type of the output | 
**technical_name** | **str** | Output technical name | 
**enum_values** | **[str]** | Values of output if the type is Enum | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


