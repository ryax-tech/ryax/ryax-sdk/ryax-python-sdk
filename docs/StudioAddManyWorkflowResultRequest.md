# StudioAddManyWorkflowResultRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**workflow_results_to_add** | [**[StudioWorkflowResult]**](StudioWorkflowResult.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


