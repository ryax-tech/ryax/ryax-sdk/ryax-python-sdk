# ryax_sdk.AuthApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorization_login_post**](AuthApi.md#authorization_login_post) | **POST** /authorization/login | Login
[**authorization_logout_get**](AuthApi.md#authorization_logout_get) | **GET** /authorization/logout | Logout
[**authorization_me_get**](AuthApi.md#authorization_me_get) | **GET** /authorization/me | Who i am
[**authorization_me_put**](AuthApi.md#authorization_me_put) | **PUT** /authorization/me | Change me


# **authorization_login_post**
> AuthorizationLoginResponse authorization_login_post()

Login

Login

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import auth_api
from ryax_sdk.model.authorization_login_request import AuthorizationLoginRequest
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_login_response import AuthorizationLoginResponse
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)
    body = AuthorizationLoginRequest(
        password="secret_password",
        username="user1",
    ) # AuthorizationLoginRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Login
        api_response = api_instance.authorization_login_post(body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling AuthApi->authorization_login_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AuthorizationLoginRequest**](AuthorizationLoginRequest.md)|  | [optional]

### Return type

[**AuthorizationLoginResponse**](AuthorizationLoginResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Login successful |  -  |
**401** | Access denied! Wrong credentials |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_logout_get**
> authorization_logout_get()

Logout

Logout

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import auth_api
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Logout
        api_instance.authorization_logout_get()
    except ryax_sdk.ApiException as e:
        print("Exception when calling AuthApi->authorization_logout_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Logout successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_me_get**
> AuthorizationUserDetails authorization_me_get()

Who i am

Auth me

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import auth_api
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_user_details import AuthorizationUserDetails
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Who i am
        api_response = api_instance.authorization_me_get()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling AuthApi->authorization_me_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthorizationUserDetails**](AuthorizationUserDetails.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User fetched successfully |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_me_put**
> AuthorizationUser authorization_me_put()

Change me

Change me

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import auth_api
from ryax_sdk.model.authorization_update_user_request import AuthorizationUpdateUserRequest
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_user import AuthorizationUser
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)
    body = AuthorizationUpdateUserRequest(
        email="user1@example.com",
        role="User",
        password="secret_password",
        username="user_1",
        comment="Testing user for project 1",
    ) # AuthorizationUpdateUserRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Change me
        api_response = api_instance.authorization_me_put(body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling AuthApi->authorization_me_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AuthorizationUpdateUserRequest**](AuthorizationUpdateUserRequest.md)|  | [optional]

### Return type

[**AuthorizationUser**](AuthorizationUser.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User updated successfully |  -  |
**400** | Cannot perform this operation |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

