# ryax_sdk.WorkflowActionsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_v2_workflows_workflow_id_modules_post**](WorkflowActionsApi.md#studio_v2_workflows_workflow_id_modules_post) | **POST** /studio/v2/workflows/{workflow_id}/modules | Add action to workflow with a link
[**studio_workflows_workflow_id_modules_post**](WorkflowActionsApi.md#studio_workflows_workflow_id_modules_post) | **POST** /studio/workflows/{workflow_id}/modules | Add action to workflow
[**studio_workflows_workflow_id_modules_workflow_module_id_change_version_post**](WorkflowActionsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_change_version_post) | **POST** /studio/workflows/{workflow_id}/modules/{workflow_module_id}/change_version | Update workflow action version
[**studio_workflows_workflow_id_modules_workflow_module_id_delete**](WorkflowActionsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_delete) | **DELETE** /studio/workflows/{workflow_id}/modules/{workflow_module_id} | Delete action from workflow
[**studio_workflows_workflow_id_modules_workflow_module_id_put**](WorkflowActionsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_put) | **PUT** /studio/workflows/{workflow_id}/modules/{workflow_module_id} | Update workflow action


# **studio_v2_workflows_workflow_id_modules_post**
> StudioWorkflowModuleExtended studio_v2_workflows_workflow_id_modules_post(workflow_id)

Add action to workflow with a link

Add a action by ID to a workflow by ID and replace if needed.

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_add_workflow_module_with_link import StudioAddWorkflowModuleWithLink
from ryax_sdk.model.studio_workflow_module_extended import StudioWorkflowModuleExtended
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_api.WorkflowActionsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    body = StudioAddWorkflowModuleWithLink(
        module_definition_id="325b9865-cf62-4dd8-8661-a5737fe7069d",
        replace_workflow_module_id="325b9865-cf62-4dd8-8661-a5737fe7069d",
        parent_workflow_module_id="325b9865-cf62-4dd8-8661-a5737fe7069d",
    ) # StudioAddWorkflowModuleWithLink |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Add action to workflow with a link
        api_response = api_instance.studio_v2_workflows_workflow_id_modules_post(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_v2_workflows_workflow_id_modules_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Add action to workflow with a link
        api_response = api_instance.studio_v2_workflows_workflow_id_modules_post(workflow_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_v2_workflows_workflow_id_modules_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **body** | [**StudioAddWorkflowModuleWithLink**](StudioAddWorkflowModuleWithLink.md)|  | [optional]

### Return type

[**StudioWorkflowModuleExtended**](StudioWorkflowModuleExtended.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** |  |  -  |
**201** | Workflow action added successfully |  -  |
**400** | Workflow not updatable |  -  |
**404** | Workflow data not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_post**
> studio_workflows_workflow_id_modules_post(workflow_id)

Add action to workflow

Add a action by ID to a workflow by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_add_workflow_module import StudioAddWorkflowModule
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_api.WorkflowActionsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    body = StudioAddWorkflowModule(
        position_x=0,
        custom_name="Video detection inside workflow",
        module_id="325b9865-cf62-4dd8-8661-a5737fe7069d",
        position_y=1,
    ) # StudioAddWorkflowModule |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Add action to workflow
        api_instance.studio_workflows_workflow_id_modules_post(workflow_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_workflows_workflow_id_modules_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Add action to workflow
        api_instance.studio_workflows_workflow_id_modules_post(workflow_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_workflows_workflow_id_modules_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **body** | [**StudioAddWorkflowModule**](StudioAddWorkflowModule.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Workflow action added successfully |  -  |
**400** | Workflow not updatable |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_change_version_post**
> StudioChangeModuleVersionResponse studio_workflows_workflow_id_modules_workflow_module_id_change_version_post(workflow_id, workflow_module_id)

Update workflow action version

Update the version of action in a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_change_module_version_response import StudioChangeModuleVersionResponse
from ryax_sdk.model.studio_change_module_version_request import StudioChangeModuleVersionRequest
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_api.WorkflowActionsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 
    body = StudioChangeModuleVersionRequest(
        module_id="30a197ce-6db1-4cc3-a895-27aeb40979dd",
    ) # StudioChangeModuleVersionRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update workflow action version
        api_response = api_instance.studio_workflows_workflow_id_modules_workflow_module_id_change_version_post(workflow_id, workflow_module_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_workflows_workflow_id_modules_workflow_module_id_change_version_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update workflow action version
        api_response = api_instance.studio_workflows_workflow_id_modules_workflow_module_id_change_version_post(workflow_id, workflow_module_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_workflows_workflow_id_modules_workflow_module_id_change_version_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |
 **body** | [**StudioChangeModuleVersionRequest**](StudioChangeModuleVersionRequest.md)|  | [optional]

### Return type

[**StudioChangeModuleVersionResponse**](StudioChangeModuleVersionResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow action version changed successfully |  -  |
**404** | Workflow or action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_delete**
> studio_workflows_workflow_id_modules_workflow_module_id_delete(workflow_id, workflow_module_id)

Delete action from workflow

Delete a action by ID from a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_api.WorkflowActionsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete action from workflow
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_delete(workflow_id, workflow_module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_workflows_workflow_id_modules_workflow_module_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow action deleted successfully |  -  |
**400** | Workflow not updatable |  -  |
**404** | Workflow or action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_put**
> studio_workflows_workflow_id_modules_workflow_module_id_put(workflow_id, workflow_module_id)

Update workflow action

Update the data of action in a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_update_workflow_module import StudioUpdateWorkflowModule
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_api.WorkflowActionsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 
    body = StudioUpdateWorkflowModule(
        position_x=0,
        custom_name="Video detection inside workflow",
        position_y=1,
    ) # StudioUpdateWorkflowModule |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update workflow action
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_put(workflow_id, workflow_module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_workflows_workflow_id_modules_workflow_module_id_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update workflow action
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_put(workflow_id, workflow_module_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsApi->studio_workflows_workflow_id_modules_workflow_module_id_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |
 **body** | [**StudioUpdateWorkflowModule**](StudioUpdateWorkflowModule.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow action updated successfully |  -  |
**400** | Workflow not updatable |  -  |
**404** | Workflow or action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

