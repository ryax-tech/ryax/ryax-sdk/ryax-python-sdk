# StudioWorkflowModule


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the action | [optional] 
**categories** | [**[StudioModuleCategory]**](StudioModuleCategory.md) | Module Categories | [optional] 
**version** | **str** | Module version | [optional] 
**description** | **str** | Module description | [optional] 
**has_dynamic_outputs** | **bool** | Flag to indicate that dynamic outputs configuration allowed or not | [optional] 
**module_id** | **str** | Module identifier | [optional] 
**position_x** | **int** | The position on x-axis of the action on the Web UI grid | [optional] 
**custom_name** | **str, none_type** | Human friendly name of the action to be displayed in the users interfaces | [optional] 
**status** | **str** | Validity status of the workflow action | [optional] 
**name** | **str** | Module name | [optional] 
**position_y** | **int** | The position on y-axis of the action on the Web UI grid | [optional] 
**deployment_status** | **str** | Deployment status of the workflow action | [optional] 
**endpoint** | **str, none_type** | Trigger endpoint. Is null if no endpoint is defined for this trigger. | [optional] 
**kind** | **str** | Module kind | [optional] 
**technical_name** | **str** | Technical name of the action | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


