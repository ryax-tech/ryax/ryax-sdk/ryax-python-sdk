# StudioWorkflowModuleInputFile


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**size** | **float** | Size of the static file | [optional] 
**id** | **str** | ID of the workflow action input static file | [optional] 
**path** | **str** | Path to static file in the filestore | [optional] 
**name** | **str** | Name of the static file | [optional] 
**extension** | **str** | Extension of the static file | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


