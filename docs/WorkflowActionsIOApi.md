# ryax_sdk.WorkflowActionsIOApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_v2_workflows_workflow_id_modules_workflow_module_id_put**](WorkflowActionsIOApi.md#studio_v2_workflows_workflow_id_modules_workflow_module_id_put) | **PUT** /studio/v2/workflows/{workflow_id}/modules/{workflow_module_id} | Update all IO of workflow action


# **studio_v2_workflows_workflow_id_modules_workflow_module_id_put**
> studio_v2_workflows_workflow_id_modules_workflow_module_id_put(workflow_id, workflow_module_id)

Update all IO of workflow action

Update all the inputs and all dynamic outputs of a workflow action

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_io_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_update_all_workflow_module_io import StudioUpdateAllWorkflowModuleIO
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_io_api.WorkflowActionsIOApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 
    body = StudioUpdateAllWorkflowModuleIO(
        dynamic_outputs=[
            StudioUpdateWorkflowModuleDynamicOutputWithId(
                enum_values=[option_1, option_2],
                id="1234",
                origin="path",
                display_name="Name",
                help="Description of the output",
                type="integer",
                technical_name="string",
            ),
        ],
        custom_name="string",
        addons_inputs=[
            StudioUpdateWorkflowModuleInputValueWithId(
                id="1234",
                static_value={},
                project_variable_value="3ff80197-c975-4f20-a0ca-c35bda9f092b",
                reference_value="3ff80197-c975-4f20-a0ca-c35bda9f092b",
            ),
        ],
        inputs=[],
    ) # StudioUpdateAllWorkflowModuleIO |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update all IO of workflow action
        api_instance.studio_v2_workflows_workflow_id_modules_workflow_module_id_put(workflow_id, workflow_module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsIOApi->studio_v2_workflows_workflow_id_modules_workflow_module_id_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update all IO of workflow action
        api_instance.studio_v2_workflows_workflow_id_modules_workflow_module_id_put(workflow_id, workflow_module_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsIOApi->studio_v2_workflows_workflow_id_modules_workflow_module_id_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |
 **body** | [**StudioUpdateAllWorkflowModuleIO**](StudioUpdateAllWorkflowModuleIO.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**400** | Workflow Action IO update error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

