# StudioUpdateWorkflowModulesLinks


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**links** | [**[StudioUpdateWorkflowModuleLinks]**](StudioUpdateWorkflowModuleLinks.md) | List of the modules with their links that you want to update. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


