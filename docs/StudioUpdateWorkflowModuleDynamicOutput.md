# StudioUpdateWorkflowModuleDynamicOutput


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enum_values** | **str** | Values of output if the type is Enum | [optional] 
**display_name** | **str** | Output name to display on Webui | [optional] 
**help** | **str** | Output help text | [optional] 
**type** | **str** | Type of the output | [optional] 
**technical_name** | **str** | Output technical name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


