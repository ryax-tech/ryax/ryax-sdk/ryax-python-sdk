# ryax_sdk.RepositoryActionsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**repository_modules_get**](RepositoryActionsApi.md#repository_modules_get) | **GET** /repository/modules | List all actions
[**repository_modules_module_id_build_post**](RepositoryActionsApi.md#repository_modules_module_id_build_post) | **POST** /repository/modules/{module_id}/build | Build repository action
[**repository_modules_module_id_delete**](RepositoryActionsApi.md#repository_modules_module_id_delete) | **DELETE** /repository/modules/{module_id} | Delete repository action


# **repository_modules_get**
> [RepositoryActionBuild] repository_modules_get()

List all actions

List all code actions available

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repository_actions_api
from ryax_sdk.model.repository_action_build import RepositoryActionBuild
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repository_actions_api.RepositoryActionsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List all actions
        api_response = api_instance.repository_modules_get()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoryActionsApi->repository_modules_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[RepositoryActionBuild]**](RepositoryActionBuild.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Action fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_modules_module_id_build_post**
> RepositoryActionBuild repository_modules_module_id_build_post(module_id)

Build repository action

Launch repository action building

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repository_actions_api
from ryax_sdk.model.repository_action_build import RepositoryActionBuild
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repository_actions_api.RepositoryActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Build repository action
        api_response = api_instance.repository_modules_module_id_build_post(module_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoryActionsApi->repository_modules_module_id_build_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

[**RepositoryActionBuild**](RepositoryActionBuild.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Module build successfully |  -  |
**400** | Unauthorized repository_action build |  -  |
**401** | Unauthorized |  -  |
**404** | Module not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_modules_module_id_delete**
> repository_modules_module_id_delete(module_id)

Delete repository action

Delete repository action

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import repository_actions_api
from ryax_sdk.model.repository_error import RepositoryError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = repository_actions_api.RepositoryActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete repository action
        api_instance.repository_modules_module_id_delete(module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling RepositoryActionsApi->repository_modules_module_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Action deleted successfully |  -  |
**400** | Action not deletable |  -  |
**404** | Action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

