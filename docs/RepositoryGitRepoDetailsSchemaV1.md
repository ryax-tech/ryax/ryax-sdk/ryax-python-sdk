# RepositoryGitRepoDetailsSchemaV1


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Identifier of the git_repo | [optional] 
**name** | **str** | Name of the git_repo | [optional] 
**url** | **str** | Source url | [optional] 
**last_scan** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | Result of the last scan of this git_repo | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


