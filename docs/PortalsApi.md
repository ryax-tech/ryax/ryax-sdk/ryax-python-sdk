# ryax_sdk.PortalsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**runner_portals_workflow_definition_id_get**](PortalsApi.md#runner_portals_workflow_definition_id_get) | **GET** /runner/portals/{workflow_definition_id} | Get a portal
[**runner_portals_workflow_definition_id_post**](PortalsApi.md#runner_portals_workflow_definition_id_post) | **POST** /runner/portals/{workflow_definition_id} | Trigger a new workflow run


# **runner_portals_workflow_definition_id_get**
> RunnerPortal runner_portals_workflow_definition_id_get(workflow_definition_id)

Get a portal

Get a portal from a workflow definition id (the id used in the studio).

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import portals_api
from ryax_sdk.model.runner_error import RunnerError
from ryax_sdk.model.runner_portal import RunnerPortal
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = portals_api.PortalsApi(api_client)
    workflow_definition_id = "workflow_definition_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get a portal
        api_response = api_instance.runner_portals_workflow_definition_id_get(workflow_definition_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling PortalsApi->runner_portals_workflow_definition_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_definition_id** | **str**|  |

### Return type

[**RunnerPortal**](RunnerPortal.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Portal fetched successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_portals_workflow_definition_id_post**
> RunnerWorkflowRunTriggerResponse runner_portals_workflow_definition_id_post(workflow_definition_id)

Trigger a new workflow run

Await a multipart, where each part is named after an input and contains the data.

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import portals_api
from ryax_sdk.model.runner_error import RunnerError
from ryax_sdk.model.runner_workflow_run_trigger_response import RunnerWorkflowRunTriggerResponse
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = portals_api.PortalsApi(api_client)
    workflow_definition_id = "workflow_definition_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Trigger a new workflow run
        api_response = api_instance.runner_portals_workflow_definition_id_post(workflow_definition_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling PortalsApi->runner_portals_workflow_definition_id_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_definition_id** | **str**|  |

### Return type

[**RunnerWorkflowRunTriggerResponse**](RunnerWorkflowRunTriggerResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Workflow run created successfully |  -  |
**400** | Malformed inputs |  -  |
**404** | Action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

