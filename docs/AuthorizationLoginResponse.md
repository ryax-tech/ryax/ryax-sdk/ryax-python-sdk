# AuthorizationLoginResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jwt** | **str** | JSON Web Token for authentication | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


