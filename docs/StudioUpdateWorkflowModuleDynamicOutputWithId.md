# StudioUpdateWorkflowModuleDynamicOutputWithId


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**origin** | **str** | Origin of this parameter | 
**display_name** | **str** | Output name to display on Webui | 
**help** | **str** | Output help text | 
**type** | **str** | Type of the output | 
**technical_name** | **str** | Output technical name | 
**enum_values** | **[str]** | Module input options for enum type | [optional] 
**id** | **str** | Output id | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


