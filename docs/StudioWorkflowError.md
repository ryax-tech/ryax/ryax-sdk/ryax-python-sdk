# StudioWorkflowError


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the workflow error | [optional] 
**code** | **int** | Workflow error message | [optional] 
**workflow_module_id** | **str, none_type** | Workflow action id (used if the error is attached to a action) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


