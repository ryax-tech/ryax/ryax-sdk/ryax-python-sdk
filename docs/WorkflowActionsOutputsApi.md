# ryax_sdk.WorkflowActionsOutputsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_portals_workflow_id_modules_workflow_module_id_outputs_get**](WorkflowActionsOutputsApi.md#studio_portals_workflow_id_modules_workflow_module_id_outputs_get) | **GET** /studio/portals/{workflow_id}/modules/{workflow_module_id}/outputs | List all workflows action outputs
[**studio_workflows_workflow_id_modules_workflow_module_id_outputs_get**](WorkflowActionsOutputsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_outputs_get) | **GET** /studio/workflows/{workflow_id}/modules/{workflow_module_id}/outputs | List all workflows action outputs
[**studio_workflows_workflow_id_modules_workflow_module_id_outputs_post**](WorkflowActionsOutputsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_outputs_post) | **POST** /studio/workflows/{workflow_id}/modules/{workflow_module_id}/outputs | Add output to workflow action
[**studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_delete**](WorkflowActionsOutputsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_delete) | **DELETE** /studio/workflows/{workflow_id}/modules/{workflow_module_id}/outputs/{workflow_dynamic_output_id} | Delete output in workflow action
[**studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_put**](WorkflowActionsOutputsApi.md#studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_put) | **PUT** /studio/workflows/{workflow_id}/modules/{workflow_module_id}/outputs/{workflow_dynamic_output_id} | Update output in workflow action


# **studio_portals_workflow_id_modules_workflow_module_id_outputs_get**
> [StudioWorkflowModuleOutput] studio_portals_workflow_id_modules_workflow_module_id_outputs_get(workflow_id, workflow_module_id)

List all workflows action outputs

List all outputs available in a workflow action

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_outputs_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_module_output import StudioWorkflowModuleOutput
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_outputs_api.WorkflowActionsOutputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # List all workflows action outputs
        api_response = api_instance.studio_portals_workflow_id_modules_workflow_module_id_outputs_get(workflow_id, workflow_module_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsOutputsApi->studio_portals_workflow_id_modules_workflow_module_id_outputs_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |

### Return type

[**[StudioWorkflowModuleOutput]**](StudioWorkflowModuleOutput.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow action outputs fetched successfully |  -  |
**404** | Workflow action output not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_outputs_get**
> [StudioWorkflowModuleOutput] studio_workflows_workflow_id_modules_workflow_module_id_outputs_get(workflow_id, workflow_module_id)

List all workflows action outputs

List all outputs available in a workflow action

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_outputs_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_module_output import StudioWorkflowModuleOutput
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_outputs_api.WorkflowActionsOutputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # List all workflows action outputs
        api_response = api_instance.studio_workflows_workflow_id_modules_workflow_module_id_outputs_get(workflow_id, workflow_module_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsOutputsApi->studio_workflows_workflow_id_modules_workflow_module_id_outputs_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |

### Return type

[**[StudioWorkflowModuleOutput]**](StudioWorkflowModuleOutput.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow action outputs fetched successfully |  -  |
**404** | Workflow action output not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_outputs_post**
> studio_workflows_workflow_id_modules_workflow_module_id_outputs_post(workflow_id, workflow_module_id)

Add output to workflow action

Create a dynamic output for a action in a workflow action

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_outputs_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_add_workflow_module_dynamic_output import StudioAddWorkflowModuleDynamicOutput
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_outputs_api.WorkflowActionsOutputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 
    body = StudioAddWorkflowModuleDynamicOutput(
        enum_values=[value1],
        display_name="Name",
        help="Description of the output",
        type="integer",
        technical_name="string",
    ) # StudioAddWorkflowModuleDynamicOutput |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Add output to workflow action
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_outputs_post(workflow_id, workflow_module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsOutputsApi->studio_workflows_workflow_id_modules_workflow_module_id_outputs_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Add output to workflow action
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_outputs_post(workflow_id, workflow_module_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsOutputsApi->studio_workflows_workflow_id_modules_workflow_module_id_outputs_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |
 **body** | [**StudioAddWorkflowModuleDynamicOutput**](StudioAddWorkflowModuleDynamicOutput.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dynamic output created successfully |  -  |
**404** | Workflow or action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_delete**
> studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_delete(workflow_id, workflow_module_id, workflow_dynamic_output_id)

Delete output in workflow action

Delete a dynamic output from workflow action

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_outputs_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_outputs_api.WorkflowActionsOutputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 
    workflow_dynamic_output_id = "workflow_dynamic_output_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete output in workflow action
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_delete(workflow_id, workflow_module_id, workflow_dynamic_output_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsOutputsApi->studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |
 **workflow_dynamic_output_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dynamic output deleted successfully |  -  |
**404** | Workflow or action or dynamic output not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_put**
> studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_put(workflow_id, workflow_module_id, workflow_dynamic_output_id)

Update output in workflow action

Update a dynamic output for a action in a workflow action

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_outputs_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_update_workflow_module_dynamic_output import StudioUpdateWorkflowModuleDynamicOutput
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_outputs_api.WorkflowActionsOutputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_id = "workflow_module_id_example" # str | 
    workflow_dynamic_output_id = "workflow_dynamic_output_id_example" # str | 
    body = StudioUpdateWorkflowModuleDynamicOutput(
        enum_values="value1",
        display_name="Name",
        help="Description of the output",
        type="integer",
        technical_name="string",
    ) # StudioUpdateWorkflowModuleDynamicOutput |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update output in workflow action
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_put(workflow_id, workflow_module_id, workflow_dynamic_output_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsOutputsApi->studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update output in workflow action
        api_instance.studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_put(workflow_id, workflow_module_id, workflow_dynamic_output_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsOutputsApi->studio_workflows_workflow_id_modules_workflow_module_id_outputs_workflow_dynamic_output_id_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_id** | **str**|  |
 **workflow_dynamic_output_id** | **str**|  |
 **body** | [**StudioUpdateWorkflowModuleDynamicOutput**](StudioUpdateWorkflowModuleDynamicOutput.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Dynamic output updated successfully |  -  |
**404** | Workflow or action or dynamic output not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

