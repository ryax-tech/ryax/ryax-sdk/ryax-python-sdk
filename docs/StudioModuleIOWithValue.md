# StudioModuleIOWithValue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enum_values** | **[str]** | Module input options for enum type | [optional] 
**id** | **str** | ID of the workflow action input | [optional] 
**default_value** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | Default value for this entry | [optional] 
**project_variable_value** | **str, none_type** |  | [optional] 
**display_name** | **str** | Workflow action input display name | [optional] 
**help** | **str** | Workflow action input help | [optional] 
**reference_value** | **str, none_type** |  | [optional] 
**static_value** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** |  | [optional] [readonly] 
**type** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] 
**technical_name** | **str** | Workflow action input technical name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


