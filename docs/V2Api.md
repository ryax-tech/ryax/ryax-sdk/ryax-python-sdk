# ryax_sdk.V2Api

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_v2_workflows_workflow_id_get**](V2Api.md#studio_v2_workflows_workflow_id_get) | **GET** /studio/v2/workflows/{workflow_id} | Get one workflow with details


# **studio_v2_workflows_workflow_id_get**
> StudioWorkflowExtended studio_v2_workflows_workflow_id_get(workflow_id)

Get one workflow with details

Get the requested workflow by ID with all details

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import v2_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_extended import StudioWorkflowExtended
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = v2_api.V2Api(api_client)
    workflow_id = "workflow_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one workflow with details
        api_response = api_instance.studio_v2_workflows_workflow_id_get(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling V2Api->studio_v2_workflows_workflow_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |

### Return type

[**StudioWorkflowExtended**](StudioWorkflowExtended.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow fetched successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

