# RepositoryGitRepoScan


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **str** | Git Repo last scan sha | [optional] 
**not_built_actions** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] [readonly] 
**id** | **str** | Scan result id | [optional] 
**scan_date** | **datetime** | Git Repo last scan date | [optional] 
**built_actions** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


