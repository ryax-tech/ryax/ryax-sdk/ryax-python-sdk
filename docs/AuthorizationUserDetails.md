# AuthorizationUserDetails


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the user | [optional] 
**email** | **str** | User&#39;s email address | [optional] 
**role** | **str** | Type of the output | [optional] 
**username** | **str** | Name of the user | [optional] 
**comment** | **str** | A comment on the user | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


