# StudioWorkflowModuleLink


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the action link | [optional] 
**input_module_id** | **str** | ID of the input action of the link | [optional] 
**output_module_id** | **str** | ID of the output action of the link | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


