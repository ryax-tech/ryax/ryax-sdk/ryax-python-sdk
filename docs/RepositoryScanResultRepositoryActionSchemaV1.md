# RepositoryScanResultRepositoryActionSchemaV1


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scan_date** | **datetime** | Source last scan date | [optional] 
**sha** | **str** | Source last scan sha | [optional] 
**none_built_modules** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] [readonly] 
**built_modules** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


