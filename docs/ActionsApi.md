# ryax_sdk.ActionsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_modules_get**](ActionsApi.md#studio_modules_get) | **GET** /studio/modules | List all actions
[**studio_modules_list_categories_get**](ActionsApi.md#studio_modules_list_categories_get) | **GET** /studio/modules/list_categories | List all action categories
[**studio_modules_module_id_delete**](ActionsApi.md#studio_modules_module_id_delete) | **DELETE** /studio/modules/{module_id} | Delete a action
[**studio_modules_module_id_get**](ActionsApi.md#studio_modules_module_id_get) | **GET** /studio/modules/{module_id} | Get one action
[**studio_modules_module_id_list_versions_get**](ActionsApi.md#studio_modules_module_id_list_versions_get) | **GET** /studio/modules/{module_id}/list_versions | Get action versions
[**studio_modules_module_id_list_versions_head**](ActionsApi.md#studio_modules_module_id_list_versions_head) | **HEAD** /studio/modules/{module_id}/list_versions | Get action versions
[**studio_modules_module_id_logo_get**](ActionsApi.md#studio_modules_module_id_logo_get) | **GET** /studio/modules/{module_id}/logo | Get one logo
[**studio_modules_module_id_logo_head**](ActionsApi.md#studio_modules_module_id_logo_head) | **HEAD** /studio/modules/{module_id}/logo | Get one logo
[**studio_static_logo_module_id_get**](ActionsApi.md#studio_static_logo_module_id_get) | **GET** /studio/static/logo/{module_id} | Get one logo
[**studio_static_logo_module_id_head**](ActionsApi.md#studio_static_logo_module_id_head) | **HEAD** /studio/static/logo/{module_id} | Get one logo


# **studio_modules_get**
> [StudioModule] studio_modules_get()

List all actions

List all actions available

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_module import StudioModule
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    search = "search_example" # str |  (optional)
    category = "category_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # List all actions
        api_response = api_instance.studio_modules_get(search=search, category=category)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_modules_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **str**|  | [optional]
 **category** | **str**|  | [optional]

### Return type

[**[StudioModule]**](StudioModule.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | actions fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_modules_list_categories_get**
> [StudioModuleCategory] studio_modules_list_categories_get()

List all action categories

List all available actions categories

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_module_category import StudioModuleCategory
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List all action categories
        api_response = api_instance.studio_modules_list_categories_get()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_modules_list_categories_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[StudioModuleCategory]**](StudioModuleCategory.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Categories fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_modules_module_id_delete**
> studio_modules_module_id_delete(module_id)

Delete a action

Delete the requested action by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_module_delete_error import StudioModuleDeleteError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete a action
        api_instance.studio_modules_module_id_delete(module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_modules_module_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow fetched successfully |  -  |
**400** | Action not deletable |  -  |
**404** | Action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_modules_module_id_get**
> StudioModuleDetails studio_modules_module_id_get(module_id)

Get one action

Get the requested action by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_module_details import StudioModuleDetails
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one action
        api_response = api_instance.studio_modules_module_id_get(module_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_modules_module_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

[**StudioModuleDetails**](StudioModuleDetails.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Action fetched successfully |  -  |
**404** | Action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_modules_module_id_list_versions_get**
> [StudioModuleVersions] studio_modules_module_id_list_versions_get(module_id)

Get action versions

Get the versions of the requested action by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_module_versions import StudioModuleVersions
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get action versions
        api_response = api_instance.studio_modules_module_id_list_versions_get(module_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_modules_module_id_list_versions_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

[**[StudioModuleVersions]**](StudioModuleVersions.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Action versions fetched successfully |  -  |
**404** | Action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_modules_module_id_list_versions_head**
> [StudioModuleVersions] studio_modules_module_id_list_versions_head(module_id)

Get action versions

Get the versions of the requested action by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_module_versions import StudioModuleVersions
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get action versions
        api_response = api_instance.studio_modules_module_id_list_versions_head(module_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_modules_module_id_list_versions_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

[**[StudioModuleVersions]**](StudioModuleVersions.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Action versions fetched successfully |  -  |
**404** | Action not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_modules_module_id_logo_get**
> studio_modules_module_id_logo_get(module_id)

Get one logo

Get the requested logo by action ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one logo
        api_instance.studio_modules_module_id_logo_get(module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_modules_module_id_logo_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Logo fetched successfully |  -  |
**404** | Logo not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_modules_module_id_logo_head**
> studio_modules_module_id_logo_head(module_id)

Get one logo

Get the requested logo by action ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one logo
        api_instance.studio_modules_module_id_logo_head(module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_modules_module_id_logo_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Logo fetched successfully |  -  |
**404** | Logo not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_static_logo_module_id_get**
> studio_static_logo_module_id_get(module_id)

Get one logo

Get the requested logo by action ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one logo
        api_instance.studio_static_logo_module_id_get(module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_static_logo_module_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Logo fetched successfully |  -  |
**404** | Logo not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_static_logo_module_id_head**
> studio_static_logo_module_id_head(module_id)

Get one logo

Get the requested logo by action ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import actions_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = actions_api.ActionsApi(api_client)
    module_id = "module_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one logo
        api_instance.studio_static_logo_module_id_head(module_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ActionsApi->studio_static_logo_module_id_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **module_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Logo fetched successfully |  -  |
**404** | Logo not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

