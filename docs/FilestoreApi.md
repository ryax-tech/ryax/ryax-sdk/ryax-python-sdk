# ryax_sdk.FilestoreApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**runner_filestore_stored_file_id_filename_get**](FilestoreApi.md#runner_filestore_stored_file_id_filename_get) | **GET** /runner/filestore/{stored_file_id}/{filename} | Download file
[**runner_filestore_stored_file_id_filename_head**](FilestoreApi.md#runner_filestore_stored_file_id_filename_head) | **HEAD** /runner/filestore/{stored_file_id}/{filename} | Download file


# **runner_filestore_stored_file_id_filename_get**
> runner_filestore_stored_file_id_filename_get(stored_file_id, filename)

Download file

Download a file from the filestore in use

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import filestore_api
from ryax_sdk.model.runner_error import RunnerError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = filestore_api.FilestoreApi(api_client)
    stored_file_id = "stored_file_id_example" # str | 
    filename = "filename_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Download file
        api_instance.runner_filestore_stored_file_id_filename_get(stored_file_id, filename)
    except ryax_sdk.ApiException as e:
        print("Exception when calling FilestoreApi->runner_filestore_stored_file_id_filename_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stored_file_id** | **str**|  |
 **filename** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | File downloaded successfully |  -  |
**404** | Error downloading file |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_filestore_stored_file_id_filename_head**
> runner_filestore_stored_file_id_filename_head(stored_file_id, filename)

Download file

Download a file from the filestore in use

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import filestore_api
from ryax_sdk.model.runner_error import RunnerError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = filestore_api.FilestoreApi(api_client)
    stored_file_id = "stored_file_id_example" # str | 
    filename = "filename_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Download file
        api_instance.runner_filestore_stored_file_id_filename_head(stored_file_id, filename)
    except ryax_sdk.ApiException as e:
        print("Exception when calling FilestoreApi->runner_filestore_stored_file_id_filename_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stored_file_id** | **str**|  |
 **filename** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | File downloaded successfully |  -  |
**404** | Error downloading file |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

