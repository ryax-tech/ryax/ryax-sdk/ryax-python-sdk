# StudioWorkflowDetails


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the workflow | [optional] 
**endpoint_prefix** | **str, none_type** | Trigger endpoint prefix associated to this Workflow | [optional] 
**description** | **str, none_type** | Description of the workflow | [optional] 
**categories** | **[str]** | All categories associated to one or more actions in the workflow | [optional] 
**modules_links** | [**[StudioWorkflowModuleLink]**](StudioWorkflowModuleLink.md) | Module links used in workflow | [optional] 
**parameters** | [**[StudioWorkflowListParameters]**](StudioWorkflowListParameters.md) | Trigger inputs representation. Only for inputs that are not null and set to a non default values | [optional] 
**status** | **str** | Validity status of the workflow | [optional] 
**modules** | [**[StudioWorkflowModule]**](StudioWorkflowModule.md) | Modules used in workflow | [optional] 
**deployment_error** | **str, none_type** | Deployment error details if any | [optional] 
**deployment_status** | **str** | deployment status of the workflow | [optional] 
**name** | **str** | Name of the workflow | [optional] 
**trigger** | **str, none_type** | Workflow trigger action name | [optional] 
**has_form** | **bool** | Whether or not this workflow has a source action with a form | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


