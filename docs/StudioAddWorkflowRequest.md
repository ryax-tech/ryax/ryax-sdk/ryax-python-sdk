# StudioAddWorkflowRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the workflow | 
**description** | **str** | Description of the workflow | [optional] 
**from_workflow** | **str** | Workflow identifier used for templating new workflow | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


