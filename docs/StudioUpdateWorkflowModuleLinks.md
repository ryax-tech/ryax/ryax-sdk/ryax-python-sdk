# StudioUpdateWorkflowModuleLinks


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**next_modules_ids** | **[str, none_type]** | List of ids for the modules you want the link to point to | 
**module_id** | **str** | ID of the action you want the links to originate from | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


