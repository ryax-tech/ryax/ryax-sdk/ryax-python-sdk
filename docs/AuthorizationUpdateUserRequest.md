# AuthorizationUpdateUserRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | User&#39;s email address | [optional] 
**role** | **str** | User&#39;s role | [optional] 
**password** | **str** | User&#39;s password | [optional] 
**username** | **str** | Username | [optional] 
**comment** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


