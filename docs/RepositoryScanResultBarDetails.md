# RepositoryScanResultBarDetails


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**modules_ready** | **int** | Number of modules not built | [optional] 
**modules_success** | **int** | Number of modules built successfully | [optional] 
**modules_error** | **int** | Number of modules in error | [optional] 
**modules_in_progress** | **int** | Number of modules building | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


