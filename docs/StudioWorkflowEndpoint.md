# StudioWorkflowEndpoint


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **str, none_type** | Path used to trigger the workflow | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


