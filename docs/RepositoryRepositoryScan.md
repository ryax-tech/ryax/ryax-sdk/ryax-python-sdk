# RepositoryRepositoryScan


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**modules** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] [readonly] 
**sha** | **str** | The commit sha of git_repo | [optional] 
**error** | **str** | Error encountered when processing git git_repo | [optional] 
**branch** | **str** | Name of scanned branch | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


