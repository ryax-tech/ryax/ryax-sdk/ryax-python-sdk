# StudioUpdateWorkflowModuleInputValue


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**static_value** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | Static value to set for the input | [optional] 
**reference_value** | **str** | Referencce value to set for the input | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


