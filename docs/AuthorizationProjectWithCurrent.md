# AuthorizationProjectWithCurrent


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the project | [optional] 
**creation_date** | **datetime** | Creation date of the project | [optional] 
**id** | **str** | ID of the project | [optional] 
**current** | **bool** | The user&#39;s current project | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


