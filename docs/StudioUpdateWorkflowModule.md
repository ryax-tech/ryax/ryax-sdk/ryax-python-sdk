# StudioUpdateWorkflowModule


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**position_x** | **int** | The position on x-axis of the action on the Web UI grid | [optional] 
**custom_name** | **str** | Human friendly name of the action to be displayed in the users interfaces | [optional] 
**position_y** | **int** | The position on y-axis of the action on the Web UI grid | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


