# ryax_sdk.WorkflowActionsLinksApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_workflows_workflow_id_modules_links_post**](WorkflowActionsLinksApi.md#studio_workflows_workflow_id_modules_links_post) | **POST** /studio/workflows/{workflow_id}/modules-links | Add action link to workflow
[**studio_workflows_workflow_id_modules_links_workflow_module_link_id_delete**](WorkflowActionsLinksApi.md#studio_workflows_workflow_id_modules_links_workflow_module_link_id_delete) | **DELETE** /studio/workflows/{workflow_id}/modules-links/{workflow_module_link_id} | Delete action link from workflow


# **studio_workflows_workflow_id_modules_links_post**
> studio_workflows_workflow_id_modules_links_post(workflow_id)

Add action link to workflow

Add a action link with input and output id to a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_links_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_add_workflow_module_link import StudioAddWorkflowModuleLink
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_links_api.WorkflowActionsLinksApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    body = StudioAddWorkflowModuleLink(
        input_module_id="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        output_module_id="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    ) # StudioAddWorkflowModuleLink |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Add action link to workflow
        api_instance.studio_workflows_workflow_id_modules_links_post(workflow_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsLinksApi->studio_workflows_workflow_id_modules_links_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Add action link to workflow
        api_instance.studio_workflows_workflow_id_modules_links_post(workflow_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsLinksApi->studio_workflows_workflow_id_modules_links_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **body** | [**StudioAddWorkflowModuleLink**](StudioAddWorkflowModuleLink.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow action link added successfully |  -  |
**400** | Workflow not updatable |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_workflows_workflow_id_modules_links_workflow_module_link_id_delete**
> studio_workflows_workflow_id_modules_links_workflow_module_link_id_delete(workflow_id, workflow_module_link_id)

Delete action link from workflow

Delete a action link by ID from a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_actions_links_api
from ryax_sdk.model.studio_error import StudioError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_actions_links_api.WorkflowActionsLinksApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    workflow_module_link_id = "workflow_module_link_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete action link from workflow
        api_instance.studio_workflows_workflow_id_modules_links_workflow_module_link_id_delete(workflow_id, workflow_module_link_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionsLinksApi->studio_workflows_workflow_id_modules_links_workflow_module_link_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **workflow_module_link_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Action link deleted successfully |  -  |
**400** | Workflow not updatable |  -  |
**404** | Workflow or action link not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

