# RunnerWorkflowRun


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the workflow run. | [optional] 
**state** | **str** | Workflow run state: [&#39;Canceled&#39;, &#39;Error&#39;, &#39;Running&#39;, &#39;Completed&#39;] | [optional] 
**started_at** | **datetime, none_type** | Workflow run start datetime | [optional] 
**last_result_at** | **datetime, none_type** | Last recorded action execution ended at this time. | [optional] 
**total_steps** | **int** | Total number of steps. | [optional] 
**completed_steps** | **int** | Number of completed steps. | [optional] 
**workflow_definition_id** | **str** | ID of the workflow this run comes from | [optional] 
**executions** | [**[RunnerModuleRun]**](RunnerModuleRun.md) | The run inputs | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


