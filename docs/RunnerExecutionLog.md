# RunnerExecutionLog


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**log** | **str** | The execution logs. | [optional] 
**id** | **str** | ID of the execution log. | [optional] 
**execution_connection_id** | **str** | ID of the execution connection. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


