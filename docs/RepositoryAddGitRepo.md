# RepositoryAddGitRepo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the git_repo | 
**url** | **str** | Source url | 
**username** | **str** | Source username | [optional] 
**password** | **str** | Source password | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


