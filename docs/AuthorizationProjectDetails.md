# AuthorizationProjectDetails


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of the project | [optional] 
**creation_date** | **datetime** | Creation date of the project | [optional] 
**id** | **str** | ID of the project | [optional] 
**users** | [**[AuthorizationUser]**](AuthorizationUser.md) | Users assined to this project | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


