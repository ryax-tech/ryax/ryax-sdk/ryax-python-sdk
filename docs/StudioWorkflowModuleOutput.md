# StudioWorkflowModuleOutput


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enum_values** | **[str]** | Module output available values for enum type | [optional] 
**id** | **str** | ID of the workflow action output | [optional] 
**display_name** | **str** | Workflow action output display name | [optional] 
**help** | **str** | Workflow action output help | [optional] 
**type** | **str** | Module output type | [optional] 
**technical_name** | **str** | Workflow action output technical name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


