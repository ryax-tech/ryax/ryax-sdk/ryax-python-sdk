# ryax_sdk.ProjectsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorization_projects_get**](ProjectsApi.md#authorization_projects_get) | **GET** /authorization/projects | List projects
[**authorization_projects_post**](ProjectsApi.md#authorization_projects_post) | **POST** /authorization/projects | Create a project
[**authorization_projects_project_id_delete**](ProjectsApi.md#authorization_projects_project_id_delete) | **DELETE** /authorization/projects/{project_id} | Delete a project
[**authorization_projects_project_id_get**](ProjectsApi.md#authorization_projects_project_id_get) | **GET** /authorization/projects/{project_id} | Get one project
[**authorization_projects_project_id_put**](ProjectsApi.md#authorization_projects_project_id_put) | **PUT** /authorization/projects/{project_id} | Update a project
[**authorization_projects_project_id_user_post**](ProjectsApi.md#authorization_projects_project_id_user_post) | **POST** /authorization/projects/{project_id}/user | Add user to project
[**authorization_projects_project_id_user_user_id_delete**](ProjectsApi.md#authorization_projects_project_id_user_user_id_delete) | **DELETE** /authorization/projects/{project_id}/user/{user_id} | Delete user from project


# **authorization_projects_get**
> [AuthorizationProject] authorization_projects_get()

List projects

List all projects

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import projects_api
from ryax_sdk.model.authorization_project import AuthorizationProject
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = projects_api.ProjectsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List projects
        api_response = api_instance.authorization_projects_get()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[AuthorizationProject]**](AuthorizationProject.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Project list fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_projects_post**
> AuthorizationAddProjectResponse authorization_projects_post()

Create a project

Create a new project

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import projects_api
from ryax_sdk.model.authorization_add_project_request import AuthorizationAddProjectRequest
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_add_project_response import AuthorizationAddProjectResponse
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = projects_api.ProjectsApi(api_client)
    body = AuthorizationAddProjectRequest(
        name="project_1",
    ) # AuthorizationAddProjectRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a project
        api_response = api_instance.authorization_projects_post(body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AuthorizationAddProjectRequest**](AuthorizationAddProjectRequest.md)|  | [optional]

### Return type

[**AuthorizationAddProjectResponse**](AuthorizationAddProjectResponse.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Project created successfully |  -  |
**400** | Project name already used |  -  |
**401** | Authentication failed |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_projects_project_id_delete**
> authorization_projects_project_id_delete(project_id)

Delete a project

Delete a project by id

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import projects_api
from ryax_sdk.model.authorization_error import AuthorizationError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = projects_api.ProjectsApi(api_client)
    project_id = "project_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete a project
        api_instance.authorization_projects_project_id_delete(project_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_project_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Project deleted successfully |  -  |
**404** | Project not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_projects_project_id_get**
> AuthorizationProjectDetails authorization_projects_project_id_get(project_id)

Get one project

Get the requested project by ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import projects_api
from ryax_sdk.model.authorization_project_details import AuthorizationProjectDetails
from ryax_sdk.model.authorization_error import AuthorizationError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = projects_api.ProjectsApi(api_client)
    project_id = "project_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get one project
        api_response = api_instance.authorization_projects_project_id_get(project_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_project_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  |

### Return type

[**AuthorizationProjectDetails**](AuthorizationProjectDetails.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Project fetched successfully |  -  |
**404** | Project not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_projects_project_id_put**
> AuthorizationProject authorization_projects_project_id_put(project_id)

Update a project

Update project data

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import projects_api
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_project import AuthorizationProject
from ryax_sdk.model.authorization_update_project_request import AuthorizationUpdateProjectRequest
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = projects_api.ProjectsApi(api_client)
    project_id = "project_id_example" # str | 
    body = AuthorizationUpdateProjectRequest(
        name="Project_name_1",
    ) # AuthorizationUpdateProjectRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Update a project
        api_response = api_instance.authorization_projects_project_id_put(project_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_project_id_put: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Update a project
        api_response = api_instance.authorization_projects_project_id_put(project_id, body=body)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_project_id_put: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  |
 **body** | [**AuthorizationUpdateProjectRequest**](AuthorizationUpdateProjectRequest.md)|  | [optional]

### Return type

[**AuthorizationProject**](AuthorizationProject.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Project updated successfully |  -  |
**404** | Project not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_projects_project_id_user_post**
> authorization_projects_project_id_user_post(project_id)

Add user to project

Add user to project

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import projects_api
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_add_user_to_project_request import AuthorizationAddUserToProjectRequest
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = projects_api.ProjectsApi(api_client)
    project_id = "project_id_example" # str | 
    body = AuthorizationAddUserToProjectRequest(
        user_id="22353926-51f5-4f44-ae74-f171371d7546",
    ) # AuthorizationAddUserToProjectRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Add user to project
        api_instance.authorization_projects_project_id_user_post(project_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_project_id_user_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Add user to project
        api_instance.authorization_projects_project_id_user_post(project_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_project_id_user_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  |
 **body** | [**AuthorizationAddUserToProjectRequest**](AuthorizationAddUserToProjectRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | User has been add successfully |  -  |
**404** | Project not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_projects_project_id_user_user_id_delete**
> authorization_projects_project_id_user_user_id_delete(project_id, user_id)

Delete user from project

Delete user from project

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import projects_api
from ryax_sdk.model.authorization_error import AuthorizationError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = projects_api.ProjectsApi(api_client)
    project_id = "project_id_example" # str | 
    user_id = "user_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Delete user from project
        api_instance.authorization_projects_project_id_user_user_id_delete(project_id, user_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ProjectsApi->authorization_projects_project_id_user_user_id_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **str**|  |
 **user_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User has been deleted successfully |  -  |
**404** | Project not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

