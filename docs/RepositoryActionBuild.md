# RepositoryActionBuild


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata_path** | **str** | Path of ryax metadata file found relative to the root git_repo | [optional] 
**sha** | **str** | The commit sha of the repository_action | [optional] 
**dynamic_outputs** | **bool** | Whether repository_action has dynamic outputs | [optional] 
**status** | **str** | Build status of the repository_action | [optional] 
**scan_errors** | **[str]** |  | [optional] 
**version** | **str** | Version of the repository_action | [optional] 
**type** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] 
**creation_date** | **datetime** | Creation date of the repository_action | [optional] 
**id** | **str** | ID of the repository_action | [optional] 
**technical_name** | **str** | Technical name of the repository_action | [optional] 
**source** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | Source of the repository_action | [optional] 
**build_error** | **str** | Buold error code and logs | [optional] 
**description** | **str** | Description of the repository_action | [optional] 
**kind** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | [optional] 
**name** | **str** | Name of the repository_action | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


