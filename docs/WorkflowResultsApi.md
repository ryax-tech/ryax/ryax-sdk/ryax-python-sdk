# ryax_sdk.WorkflowResultsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**runner_results_workflow_run_id_get**](WorkflowResultsApi.md#runner_results_workflow_run_id_get) | **GET** /runner/results/{workflow_run_id} | Get the results for a workflow with user auth
[**runner_results_workflow_run_id_head**](WorkflowResultsApi.md#runner_results_workflow_run_id_head) | **HEAD** /runner/results/{workflow_run_id} | Get the results for a workflow with user auth
[**runner_run_results_workflow_run_id_get**](WorkflowResultsApi.md#runner_run_results_workflow_run_id_get) | **GET** /runner/run-results/{workflow_run_id} | Get the results for a workflow
[**runner_run_results_workflow_run_id_head**](WorkflowResultsApi.md#runner_run_results_workflow_run_id_head) | **HEAD** /runner/run-results/{workflow_run_id} | Get the results for a workflow


# **runner_results_workflow_run_id_get**
> runner_results_workflow_run_id_get(workflow_run_id)

Get the results for a workflow with user auth

Get all the results for a workflow by the workflow run ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_results_api
from ryax_sdk.model.runner_error import RunnerError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_results_api.WorkflowResultsApi(api_client)
    workflow_run_id = "workflow_run_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get the results for a workflow with user auth
        api_instance.runner_results_workflow_run_id_get(workflow_run_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowResultsApi->runner_results_workflow_run_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_run_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A dictionary of keys/values. |  -  |
**404** | Workflow results or workflow run not found |  -  |
**422** | Workflow run has failed or was canceled. |  -  |
**503** | Workflow results not ready yet |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_results_workflow_run_id_head**
> runner_results_workflow_run_id_head(workflow_run_id)

Get the results for a workflow with user auth

Get all the results for a workflow by the workflow run ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_results_api
from ryax_sdk.model.runner_error import RunnerError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_results_api.WorkflowResultsApi(api_client)
    workflow_run_id = "workflow_run_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get the results for a workflow with user auth
        api_instance.runner_results_workflow_run_id_head(workflow_run_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowResultsApi->runner_results_workflow_run_id_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_run_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A dictionary of keys/values. |  -  |
**404** | Workflow results or workflow run not found |  -  |
**422** | Workflow run has failed or was canceled. |  -  |
**503** | Workflow results not ready yet |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_run_results_workflow_run_id_get**
> runner_run_results_workflow_run_id_get(workflow_run_id)

Get the results for a workflow

Get all the results for a workflow by the workflow run ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_results_api
from ryax_sdk.model.runner_error import RunnerError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_results_api.WorkflowResultsApi(api_client)
    workflow_run_id = "workflow_run_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get the results for a workflow
        api_instance.runner_run_results_workflow_run_id_get(workflow_run_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowResultsApi->runner_run_results_workflow_run_id_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_run_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A dictionary of keys/values. |  -  |
**404** | Workflow results or workflow run not found |  -  |
**422** | Workflow run has failed or was canceled. |  -  |
**503** | Workflow results not ready yet |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_run_results_workflow_run_id_head**
> runner_run_results_workflow_run_id_head(workflow_run_id)

Get the results for a workflow

Get all the results for a workflow by the workflow run ID

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_results_api
from ryax_sdk.model.runner_error import RunnerError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_results_api.WorkflowResultsApi(api_client)
    workflow_run_id = "workflow_run_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get the results for a workflow
        api_instance.runner_run_results_workflow_run_id_head(workflow_run_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowResultsApi->runner_run_results_workflow_run_id_head: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_run_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A dictionary of keys/values. |  -  |
**404** | Workflow results or workflow run not found |  -  |
**422** | Workflow run has failed or was canceled. |  -  |
**503** | Workflow results not ready yet |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

