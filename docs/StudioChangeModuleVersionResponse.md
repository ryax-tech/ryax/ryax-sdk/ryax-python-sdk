# StudioChangeModuleVersionResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**workflow_module_id** | **str** | Identifier of the new workflow action | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


