# StudioUpdateAllWorkflowModuleIO


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dynamic_outputs** | [**[StudioUpdateWorkflowModuleDynamicOutputWithId]**](StudioUpdateWorkflowModuleDynamicOutputWithId.md) |  | [optional] 
**custom_name** | **str** | Module new name | [optional] 
**addons_inputs** | [**[StudioUpdateWorkflowModuleInputValueWithId]**](StudioUpdateWorkflowModuleInputValueWithId.md) |  | [optional] 
**inputs** | [**[StudioUpdateWorkflowModuleInputValueWithId]**](StudioUpdateWorkflowModuleInputValueWithId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


