# ryax_sdk.WorkflowActionOutputsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**studio_workflows_workflow_id_modules_outputs_get**](WorkflowActionOutputsApi.md#studio_workflows_workflow_id_modules_outputs_get) | **GET** /studio/workflows/{workflow_id}/modules-outputs | Search in all action outputs available in workflow


# **studio_workflows_workflow_id_modules_outputs_get**
> [StudioWorkflowModuleOutput] studio_workflows_workflow_id_modules_outputs_get(workflow_id)

Search in all action outputs available in workflow

Search in all action outputs available in a workflow

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import workflow_action_outputs_api
from ryax_sdk.model.studio_error import StudioError
from ryax_sdk.model.studio_workflow_module_output import StudioWorkflowModuleOutput
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = workflow_action_outputs_api.WorkflowActionOutputsApi(api_client)
    workflow_id = "workflow_id_example" # str | 
    with_type = "with_type_example" # str | Filter workflow actions outputs by type (optional)
    accessible_from = "accessible_from_example" # str | Filter workflow actions outputs accessible from specified workflow action id (optional)

    # example passing only required values which don't have defaults set
    try:
        # Search in all action outputs available in workflow
        api_response = api_instance.studio_workflows_workflow_id_modules_outputs_get(workflow_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionOutputsApi->studio_workflows_workflow_id_modules_outputs_get: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Search in all action outputs available in workflow
        api_response = api_instance.studio_workflows_workflow_id_modules_outputs_get(workflow_id, with_type=with_type, accessible_from=accessible_from)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling WorkflowActionOutputsApi->studio_workflows_workflow_id_modules_outputs_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  |
 **with_type** | **str**| Filter workflow actions outputs by type | [optional]
 **accessible_from** | **str**| Filter workflow actions outputs accessible from specified workflow action id | [optional]

### Return type

[**[StudioWorkflowModuleOutput]**](StudioWorkflowModuleOutput.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Workflow action outputs fetched successfully |  -  |
**404** | Workflow not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

