# ryax_sdk.MonitoringApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorization_healthz_get**](MonitoringApi.md#authorization_healthz_get) | **GET** /authorization/healthz | Check service status
[**repository_healthz_get**](MonitoringApi.md#repository_healthz_get) | **GET** /repository/healthz | Check service status
[**runner_healthz_get**](MonitoringApi.md#runner_healthz_get) | **GET** /runner/healthz | Check service status
[**studio_healthz_get**](MonitoringApi.md#studio_healthz_get) | **GET** /studio/healthz | Check service status


# **authorization_healthz_get**
> authorization_healthz_get()

Check service status

Help to know service status

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import monitoring_api
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = monitoring_api.MonitoringApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Check service status
        api_instance.authorization_healthz_get()
    except ryax_sdk.ApiException as e:
        print("Exception when calling MonitoringApi->authorization_healthz_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Service healthy |  -  |
**400** | Service unhealthy |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **repository_healthz_get**
> repository_healthz_get()

Check service status

Help to know service status

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import monitoring_api
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = monitoring_api.MonitoringApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Check service status
        api_instance.repository_healthz_get()
    except ryax_sdk.ApiException as e:
        print("Exception when calling MonitoringApi->repository_healthz_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Service healthy |  -  |
**400** | Service unhealthy |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **runner_healthz_get**
> runner_healthz_get()

Check service status

Help to know service status

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import monitoring_api
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = monitoring_api.MonitoringApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Check service status
        api_instance.runner_healthz_get()
    except ryax_sdk.ApiException as e:
        print("Exception when calling MonitoringApi->runner_healthz_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Service healthy |  -  |
**400** | Service unhealthy |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **studio_healthz_get**
> studio_healthz_get()

Check service status

Help to know service status

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import monitoring_api
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = monitoring_api.MonitoringApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Check service status
        api_instance.studio_healthz_get()
    except ryax_sdk.ApiException as e:
        print("Exception when calling MonitoringApi->studio_healthz_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Service healthy |  -  |
**400** | Service unhealthy |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

