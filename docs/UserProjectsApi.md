# ryax_sdk.UserProjectsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorization_projects_users_user_id_current_get**](UserProjectsApi.md#authorization_projects_users_user_id_current_get) | **GET** /authorization/projects/users/{user_id}/current | Get current project
[**authorization_projects_users_user_id_current_post**](UserProjectsApi.md#authorization_projects_users_user_id_current_post) | **POST** /authorization/projects/users/{user_id}/current | Set current project
[**authorization_projects_users_user_id_projects_get**](UserProjectsApi.md#authorization_projects_users_user_id_projects_get) | **GET** /authorization/projects/users/{user_id}/projects | Get user projects
[**authorization_v2_projects_current_post**](UserProjectsApi.md#authorization_v2_projects_current_post) | **POST** /authorization/v2/projects/current | Set current project
[**authorization_v2_projects_get**](UserProjectsApi.md#authorization_v2_projects_get) | **GET** /authorization/v2/projects | Get user projects with current one
[**authorization_v2_projects_head**](UserProjectsApi.md#authorization_v2_projects_head) | **HEAD** /authorization/v2/projects | Get user projects with current one


# **authorization_projects_users_user_id_current_get**
> AuthorizationProject authorization_projects_users_user_id_current_get(user_id)

Get current project

Get current project

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import user_projects_api
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_project import AuthorizationProject
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = user_projects_api.UserProjectsApi(api_client)
    user_id = "user_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get current project
        api_response = api_instance.authorization_projects_users_user_id_current_get(user_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UserProjectsApi->authorization_projects_users_user_id_current_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  |

### Return type

[**AuthorizationProject**](AuthorizationProject.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User current project fetched successfully |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_projects_users_user_id_current_post**
> authorization_projects_users_user_id_current_post(user_id)

Set current project

Set current project

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import user_projects_api
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_set_current_project_request import AuthorizationSetCurrentProjectRequest
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = user_projects_api.UserProjectsApi(api_client)
    user_id = "user_id_example" # str | 
    body = AuthorizationSetCurrentProjectRequest(
        project_id="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    ) # AuthorizationSetCurrentProjectRequest |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Set current project
        api_instance.authorization_projects_users_user_id_current_post(user_id)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UserProjectsApi->authorization_projects_users_user_id_current_post: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Set current project
        api_instance.authorization_projects_users_user_id_current_post(user_id, body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UserProjectsApi->authorization_projects_users_user_id_current_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  |
 **body** | [**AuthorizationSetCurrentProjectRequest**](AuthorizationSetCurrentProjectRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | User current project set successfully |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_projects_users_user_id_projects_get**
> [AuthorizationProject] authorization_projects_users_user_id_projects_get(user_id)

Get user projects

Get user's accessible projects

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import user_projects_api
from ryax_sdk.model.authorization_error import AuthorizationError
from ryax_sdk.model.authorization_project import AuthorizationProject
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = user_projects_api.UserProjectsApi(api_client)
    user_id = "user_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get user projects
        api_response = api_instance.authorization_projects_users_user_id_projects_get(user_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UserProjectsApi->authorization_projects_users_user_id_projects_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_id** | **str**|  |

### Return type

[**[AuthorizationProject]**](AuthorizationProject.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User projects fetched successfully |  -  |
**404** | User not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_v2_projects_current_post**
> authorization_v2_projects_current_post()

Set current project

Set current project

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import user_projects_api
from ryax_sdk.model.authorization_set_current_project_request import AuthorizationSetCurrentProjectRequest
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = user_projects_api.UserProjectsApi(api_client)
    body = AuthorizationSetCurrentProjectRequest(
        project_id="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    ) # AuthorizationSetCurrentProjectRequest |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Set current project
        api_instance.authorization_v2_projects_current_post(body=body)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UserProjectsApi->authorization_v2_projects_current_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AuthorizationSetCurrentProjectRequest**](AuthorizationSetCurrentProjectRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | User current project set successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_v2_projects_get**
> [AuthorizationProjectWithCurrent] authorization_v2_projects_get()

Get user projects with current one

Get user's accessible projects and highlight the one currently selected

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import user_projects_api
from ryax_sdk.model.authorization_project_with_current import AuthorizationProjectWithCurrent
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = user_projects_api.UserProjectsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Get user projects with current one
        api_response = api_instance.authorization_v2_projects_get()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UserProjectsApi->authorization_v2_projects_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[AuthorizationProjectWithCurrent]**](AuthorizationProjectWithCurrent.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User projects fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **authorization_v2_projects_head**
> [AuthorizationProjectWithCurrent] authorization_v2_projects_head()

Get user projects with current one

Get user's accessible projects and highlight the one currently selected

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import user_projects_api
from ryax_sdk.model.authorization_project_with_current import AuthorizationProjectWithCurrent
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = user_projects_api.UserProjectsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Get user projects with current one
        api_response = api_instance.authorization_v2_projects_head()
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling UserProjectsApi->authorization_v2_projects_head: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[AuthorizationProjectWithCurrent]**](AuthorizationProjectWithCurrent.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User projects fetched successfully |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

