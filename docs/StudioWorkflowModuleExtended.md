# StudioWorkflowModuleExtended


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addons** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | Addons parameters | [optional] 
**inputs** | [**[StudioModuleIOWithValue]**](StudioModuleIOWithValue.md) | Modules inputs | [optional] 
**id** | **str** | ID of the action | [optional] 
**categories** | [**[StudioModuleCategory]**](StudioModuleCategory.md) | Module Categories | [optional] 
**version** | **str** | Module version | [optional] 
**description** | **str** | Description of the action | [optional] 
**has_dynamic_outputs** | **bool** | True if this action has dynamic outputs | [optional] 
**module_id** | **str** | Action identifier | [optional] 
**versions** | [**[StudioModuleVersions]**](StudioModuleVersions.md) |  | [optional] 
**status** | **str** | Validity status of the workflow action | [optional] 
**custom_name** | **str, none_type** | Human friendly name of the action to be displayed in the users interfaces | [optional] 
**addons_inputs** | [**[StudioModuleIOWithValue]**](StudioModuleIOWithValue.md) | Module addons inputs | [optional] 
**name** | **str** | Name of the action | [optional] 
**dynamic_outputs** | [**[StudioModuleIOSchemaWithOrigin]**](StudioModuleIOSchemaWithOrigin.md) | Module dynamic outputs | [optional] 
**kind** | **str** | Module kind | [optional] 
**technical_name** | **str** | Technical name of the action | [optional] 
**outputs** | [**[StudioModuleIO]**](StudioModuleIO.md) | Module outputs | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


