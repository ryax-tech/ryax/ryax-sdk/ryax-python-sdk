# StudioWorkflowModuleInput


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enum_values** | **[str]** | Module input available options for enum type | [optional] 
**reference_output** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | Definition of the workflow action output referenced | [optional] 
**id** | **str** | ID of the workflow action input | [optional] 
**workflow_file** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}, none_type** | Metadata of the input static file | [optional] 
**display_name** | **str** | Workflow action input display name | [optional] 
**help** | **str** | Workflow action input help | [optional] 
**reference_value** | **str, none_type** | Module input value to reference other output | [optional] 
**static_value** | **str, none_type** | Module input value to reference other input | [optional] 
**type** | **str** | Module input type | [optional] 
**technical_name** | **str** | Workflow action input technical name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


