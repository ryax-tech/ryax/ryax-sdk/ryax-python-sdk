# StudioChangeModuleVersionRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**module_id** | **str** | The new action id that correspond to the version to update | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


