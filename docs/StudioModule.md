# StudioModule


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of the action | [optional] 
**version** | **str** | Module version | [optional] 
**description** | **str** | Description of the action | [optional] 
**name** | **str** | Name of the action | [optional] 
**kind** | **str** | Module kind | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


