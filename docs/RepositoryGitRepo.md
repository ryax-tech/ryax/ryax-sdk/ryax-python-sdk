# RepositoryGitRepo


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Identifier of the git_repo | [optional] 
**name** | **str** | Name of the git_repo | [optional] 
**url** | **str** | URL of this git repo | [optional] 
**last_scan** | [**RepositoryGitRepoScan**](RepositoryGitRepoScan.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


