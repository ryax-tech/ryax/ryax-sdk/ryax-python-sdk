# ryax_sdk.ExecutionLogsApi

All URIs are relative to *https://app.ryax.tech*

Method | HTTP request | Description
------------- | ------------- | -------------
[**runner_workflow_runs_execution_result_id_logs_get**](ExecutionLogsApi.md#runner_workflow_runs_execution_result_id_logs_get) | **GET** /runner/workflow_runs/{execution_result_id}/logs | Get an execution log


# **runner_workflow_runs_execution_result_id_logs_get**
> RunnerExecutionLog runner_workflow_runs_execution_result_id_logs_get(execution_result_id)

Get an execution log

Get the logs for an execution result with its ID.

### Example

* Api Key Authentication (bearer):
```python
import time
import ryax_sdk
from ryax_sdk.api import execution_logs_api
from ryax_sdk.model.runner_execution_log import RunnerExecutionLog
from ryax_sdk.model.runner_error import RunnerError
from pprint import pprint
# Defining the host is optional and defaults to https://app.ryax.tech
# See configuration.py for a list of all supported configuration parameters.
configuration = ryax_sdk.Configuration(
    host = "https://app.ryax.tech"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: bearer
configuration.api_key['bearer'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['bearer'] = 'Bearer'

# Enter a context with an instance of the API client
with ryax_sdk.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = execution_logs_api.ExecutionLogsApi(api_client)
    execution_result_id = "execution_result_id_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get an execution log
        api_response = api_instance.runner_workflow_runs_execution_result_id_logs_get(execution_result_id)
        pprint(api_response)
    except ryax_sdk.ApiException as e:
        print("Exception when calling ExecutionLogsApi->runner_workflow_runs_execution_result_id_logs_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **execution_result_id** | **str**|  |

### Return type

[**RunnerExecutionLog**](RunnerExecutionLog.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Execution Log fetched successfully |  -  |
**404** | Execution Log not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

