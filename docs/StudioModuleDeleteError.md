# StudioModuleDeleteError


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**workflows** | [**[StudioLightWorkflow]**](StudioLightWorkflow.md) | List of the workflows where the action is used | [optional] 
**code** | **int** | Value of the error code | [optional]  if omitted the server will use the default value of 1
**error** | **str** | Error name | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


